﻿using ERP.WebApi.Business.Features.Supplier.Commands;
using ERP.WebApi.Business.Features.Supplier.Queries;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Management
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class SupplierController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SupplierController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(SupplierGetRecList))]
        public async Task<ActionResult<List<SupplierListItem>>> SupplierGetRecList()
        {
            var listItems = await _mediator.Send(new SupplierGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]/{supplierId?}", Name = nameof(SupplierGetRec))]
        public async Task<ActionResult<SupplierDto>> SupplierGetRec(int supplierId = 0)
        {
            if (supplierId == 0) return Ok();
            var supplierDto = await _mediator.Send(new SupplierGetRecCommand { SupplierId = supplierId });
            return Ok(supplierDto);
        }

        [HttpPost("[action]", Name = nameof(SupplierAddRec))]
        public async Task<ActionResult> SupplierAddRec([FromBody] SupplierAddRecCommand supplierAddRecCommand)
        {
            var addRecResult = await _mediator.Send(supplierAddRecCommand);
            return Ok(addRecResult);
        }

        [HttpPost("[action]", Name = nameof(SupplierModiRec))]
        public async Task<ActionResult> SupplierModiRec([FromBody] SupplierModiRecCommand supplierModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(supplierModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}