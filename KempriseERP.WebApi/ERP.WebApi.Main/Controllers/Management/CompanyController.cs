﻿using ERP.WebApi.Business.Features.Company.Commands;
using ERP.WebApi.Business.Features.Company.Queries;
using ERP.WebApi.DTO.DTOs.Management.Company;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Management
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CompanyController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CompanyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(CompanyGetRecList))]
        public async Task<ActionResult<List<CompanyListItem>>> CompanyGetRecList()
        {
            var listItems = await _mediator.Send(new CompanyGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]/{companyId?}", Name = nameof(CompanyGetRec))]
        public async Task<ActionResult<CompanyDto>> CompanyGetRec(int companyId = 0)
        {
            if (companyId == 0) return Ok();
            var companyDto = await _mediator.Send(new CompanyGetRecCommand { CompanyId = companyId });
            return Ok(companyDto);
        }

        [HttpPost("[action]", Name = nameof(CompanyAddRec))]
        public async Task<ActionResult> CompanyAddRec([FromBody] CompanyAddRecCommand companyAddRecCommand)
        {
            var addRecResult = await _mediator.Send(companyAddRecCommand);
            return Ok(addRecResult);
        }

        [HttpPost("[action]", Name = nameof(CompanyModiRec))]
        public async Task<ActionResult> CompanyModiRec([FromBody] CompanyModiRecCommand companyModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(companyModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}