﻿using ERP.WebApi.Business.Features.Employee.Commands;
using ERP.WebApi.Business.Features.Employee.Queries;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Management
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class EmployeeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public EmployeeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(EmployeeGetRecList))]
        public async Task<ActionResult<List<EmployeeListItem>>> EmployeeGetRecList()
        {
            var listItems = await _mediator.Send(new EmployeeGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]/{employeeId?}", Name = nameof(EmployeeGetRec))]
        public async Task<ActionResult<EmployeeAggregate>> EmployeeGetRec(int employeeId = 0)
        {
            if (employeeId == 0) return Ok();
            var employeeAggregate = await _mediator.Send(new EmployeeGetRecCommand { EmployeeId = employeeId });
            return Ok(employeeAggregate);
        }

        [HttpPost("[action]", Name = nameof(EmployeeAddRec))]
        public async Task<ActionResult> EmployeeAddRec([FromBody] EmployeeAddRecCommand employeeAddRecCommand)
        {
            var addRecResult = await _mediator.Send(employeeAddRecCommand);
            return Ok(addRecResult);
        }

        [HttpPost("[action]", Name = nameof(EmployeeModiRec))]
        public async Task<ActionResult> EmployeeModiRec([FromBody] EmployeeModiRecCommand employeeModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(employeeModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}