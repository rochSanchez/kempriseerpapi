﻿using ERP.WebApi.Business.Features.Currency.Commands;
using ERP.WebApi.Business.Features.Currency.Queries;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Management
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CurrencyController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CurrencyController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(CurrencyGetRecList))]
        public async Task<ActionResult<List<CurrencyListItem>>> CurrencyGetRecList()
        {
            var listItems = await _mediator.Send(new CurrencyGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]/{currencyId?}", Name = nameof(CurrencyGetRec))]
        public async Task<ActionResult<CurrencyDto>> CurrencyGetRec(int currencyId = 0)
        {
            if (currencyId == 0) return Ok();
            var currencyDto = await _mediator.Send(new CurrencyGetRecCommand { CurrencyId = currencyId });
            return Ok(currencyDto);
        }

        [HttpPost("[action]", Name = nameof(CurrencyAddRec))]
        public async Task<ActionResult> CurrencyAddRec([FromBody] CurrencyAddRecCommand currencyAddRecCommand)
        {
            var addRecResult = await _mediator.Send(currencyAddRecCommand);
            return Ok(addRecResult);
        }

        [HttpPost("[action]", Name = nameof(CurrencyModiRec))]
        public async Task<ActionResult> CurrencyModiRec([FromBody] CurrencyModiRecCommand currencyModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(currencyModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}