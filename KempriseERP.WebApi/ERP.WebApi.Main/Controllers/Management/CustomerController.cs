﻿using ERP.WebApi.Business.Features.Customers.Commands;
using ERP.WebApi.Business.Features.Customers.Queries;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Management
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CustomerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(CustomerGetRecList))]
        public async Task<ActionResult<List<CustomerListItem>>> CustomerGetRecList()
        {
            var listItems = await _mediator.Send(new CustomerGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]/{customerId?}", Name = nameof(CustomerGetRec))]
        public async Task<ActionResult<CustomerDto>> CustomerGetRec(int customerId = 0)
        {
            if (customerId == 0) return Ok();
            var customerDto = await _mediator.Send(new CustomerGetRecCommand { CustomerId = customerId });
            return Ok(customerDto);
        }

        [HttpPost("[action]", Name = nameof(CustomerAddRec))]
        public async Task<ActionResult> CustomerAddRec([FromBody] CustomerAddRecCommand customerAddRecCommand)
        {
            var addRecResult = await _mediator.Send(customerAddRecCommand);
            return Ok(addRecResult);
        }

        [HttpPost("[action]", Name = nameof(CustomerModiRec))]
        public async Task<ActionResult> CustomerModiRec([FromBody] CustomerModiRecCommand customerModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(customerModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}