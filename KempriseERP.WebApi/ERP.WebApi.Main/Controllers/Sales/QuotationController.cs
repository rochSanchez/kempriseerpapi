﻿using ERP.WebApi.Business.Features.Inquiry.Queries;
using ERP.WebApi.Business.Features.Quotation.Commands;
using ERP.WebApi.Business.Features.Quotation.Queries;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.DTO.Constants;

namespace ERP.WebApi.Main.Controllers.Sales
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class QuotationController : ControllerBase
    {
        private readonly IMediator _mediator;

        public QuotationController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(QuotationGetRecList))]
        public async Task<ActionResult<List<QuotationListItem>>> QuotationGetRecList()
        {
            var listItems = await _mediator.Send(new QuotationGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]", Name = nameof(QuotationInquiryGetRecList))]
        public async Task<ActionResult<List<InquiryListItem>>> QuotationInquiryGetRecList()
        {
            var listItems = await _mediator.Send(new InquiryGetRecListCommand());
            var pendingReviewItems = listItems.Where(x => x.Status == InquiryStatus.PendingReview);
            return Ok(pendingReviewItems);
        }

        [HttpGet("[action]/{quotationId?}", Name = nameof(QuotationGetRec))]
        public async Task<ActionResult<QuotationAggregate>> QuotationGetRec(int quotationId = 0)
        {
            if (quotationId == 0) return Ok();
            var quotationAggregate = await _mediator.Send(new QuotationGetRecCommand { QuotationId = quotationId });
            return Ok(quotationAggregate);
        }

        [HttpPost("[action]", Name = nameof(QuotationAddRec))]
        public async Task<ActionResult> QuotationAddRec([FromBody] QuotationAddRecCommand quotationAddRecCommand)
        {
            var addRecResult = await _mediator.Send(quotationAddRecCommand);
            return Ok(addRecResult.Entity);
        }

        [HttpPost("[action]", Name = nameof(QuotationModiRec))]
        public async Task<ActionResult> QuotationModiRec([FromBody] QuotationModiRecCommand quotationModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(quotationModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}