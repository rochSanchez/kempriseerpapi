﻿using ERP.WebApi.Business.Features.Inquiry.Commands;
using ERP.WebApi.Business.Features.Inquiry.Queries;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Sales
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class InquiryController : ControllerBase
    {
        private readonly IMediator _mediator;

        public InquiryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Authorize(Roles = "SUPER USER, SALES ADMIN, SALES ENGINEER, SENIOR SALES, SALES MANAGER")]
        [HttpGet("[action]", Name = nameof(InquiryGetRecList))]
        public async Task<ActionResult<List<InquiryListItem>>> InquiryGetRecList()
        {
            var listItems = await _mediator.Send(new InquiryGetRecListCommand());
            return Ok(listItems);
        }

        [Authorize(Roles = "SUPER USER, SALES ADMIN, SALES ENGINEER, SENIOR SALES, SALES MANAGER")]
        [HttpGet("[action]/{inquiryId?}", Name = nameof(InquiryGetRec))]
        public async Task<ActionResult<InquiryAggregate>> InquiryGetRec(int inquiryId = 0)
        {
            if (inquiryId == 0) return Ok();
            var inquiryAggregate = await _mediator.Send(new InquiryGetRecCommand { InquiryId = inquiryId });
            return Ok(inquiryAggregate);
        }

        [Authorize(Roles = "SUPER USER, SALES ADMIN, SALES ENGINEER, SENIOR SALES, SALES MANAGER")]
        [HttpPost("[action]", Name = nameof(InquiryAddRec))]
        public async Task<ActionResult> InquiryAddRec([FromBody] InquiryAddRecCommand inquiryAddRecCommand)
        {
            var addRecResult = await _mediator.Send(inquiryAddRecCommand);
            return Ok(addRecResult.Entity);
        }

        [Authorize(Roles = "SUPER USER, SALES ADMIN, SALES ENGINEER, SENIOR SALES, SALES MANAGER")]
        [HttpPost("[action]", Name = nameof(InquiryModiRec))]
        public async Task<ActionResult> InquiryModiRec([FromBody] InquiryModiRecCommand inquiryModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(inquiryModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}