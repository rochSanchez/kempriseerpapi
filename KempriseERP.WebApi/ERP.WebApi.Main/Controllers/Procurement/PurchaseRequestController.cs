﻿using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.WebApi.Business.Features.PORequest.Commands;
using ERP.WebApi.Business.Features.PORequest.Queries;
using ERP.WebApi.DTO.DTOs.Procurement.Request;

namespace ERP.WebApi.Main.Controllers.Procurement
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PurchaseRequestController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PurchaseRequestController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(PurchaseRequestGetRecList))]
        public async Task<ActionResult<List<PurchaseOrderRequestListItem>>> PurchaseRequestGetRecList()
        {
            var listItems = await _mediator.Send(new PurchaseOrderRequestGetRecListCommand());
            return Ok(listItems);
        }

        [HttpGet("[action]/{puchaseRequestId?}", Name = nameof(PurchaseRequestGetRec))]
        public async Task<ActionResult<PoRequestAggregate>> PurchaseRequestGetRec(int puchaseRequestId = 0)
        {
            if (puchaseRequestId == 0) return Ok();
            var inquiryAggregate = await _mediator.Send(new PurchaseOrderRequestGetRecCommand { PuchaseRequestId = puchaseRequestId });
            return Ok(inquiryAggregate);
        }

        [HttpPost("[action]", Name = nameof(PurchaseRequestAddRec))]
        public async Task<ActionResult> PurchaseRequestAddRec([FromBody] PurchaseRequestAddRecCommand purchaseRequestAddRecCommand)
        {
            var addRecResult = await _mediator.Send(purchaseRequestAddRecCommand);
            return Ok(addRecResult.Entity);
        }

        [HttpPost("[action]", Name = nameof(PurchaseRequestModiRec))]
        public async Task<ActionResult> PurchaseRequestModiRec([FromBody] PurchaseRequestModiRecCommand purchaseRequestModiRecCommand)
        {
            var modiRecResult = await _mediator.Send(purchaseRequestModiRecCommand);
            return Ok(modiRecResult);
        }
    }
}