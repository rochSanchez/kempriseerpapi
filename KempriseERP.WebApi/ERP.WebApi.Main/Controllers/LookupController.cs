﻿using ERP.WebApi.Business.Features.Lookups;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class LookupController : ControllerBase
    {
        private readonly IMediator _mediator;

        public LookupController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet("[action]", Name = nameof(ManagementGetLookups))]
        public async Task<ActionResult<ManagementLookupAggregate>> ManagementGetLookups()
        {
            var lookupAggregate = await _mediator.Send(new ManagementGetLookupsCommand());
            return Ok(lookupAggregate);
        }

        [HttpGet("[action]", Name = nameof(SalesGetLookups))]
        public async Task<ActionResult<SalesLookupAggregate>> SalesGetLookups()
        {
            var lookupAggregate = await _mediator.Send(new SalesGetLookupsCommand());
            return Ok(lookupAggregate);
        }
    }
}