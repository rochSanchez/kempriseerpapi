﻿using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Security.Authentication;
using ERP.WebApi.DTO.Security.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ERP.WebApi.Main.Controllers.Authentication
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUserManagementService _userManagementService;

        public AccountController(IAuthenticationService authenticationService, IUserManagementService userManagementService)
        {
            _authenticationService = authenticationService;
            _userManagementService = userManagementService;
        }

        [Authorize(Roles = "SUPER USER")]
        [HttpPost("Register")]
        public async Task<ActionResult<RegistrationResponse>> RegisterAsync([FromBody]RegistrationRequest request)
        {
            return Ok(await _authenticationService.RegisterAsync(request));
        }

        [HttpPost("Authenticate")]
        public async Task<ActionResult<AuthenticationResponse>> AuthenticateAsync(AuthenticationRequest request)
        {
            return Ok(await _authenticationService.AuthenticateAsync(request));
        }

        [Authorize(Roles = "SUPER USER")]
        [HttpPost("AddRole")]
        public async Task<ActionResult<string>> AddRoleAsync(AddRoleRequest addRoleRequest)
        {
            return Ok(await _userManagementService.CreateRoleAsync(addRoleRequest.RoleName));
        }

        [Authorize(Roles = "SUPER USER, , SALES MANAGER")]
        [HttpPost("AddUserToRole")]
        public async Task<ActionResult<string>> AddRoleUserToAsync(UserToRoleDto userToRoleDto)
        {
            return Ok(await _userManagementService.AddUserToRoleAsync(userToRoleDto));
        }

        [Authorize(Roles = "SUPER USER, , SALES MANAGER")]
        [HttpPost("RemoveUserFromRole")]
        public async Task<ActionResult<string>> RemoveUserFromRoleAsync(UserToRoleDto userToRoleDto)
        {
            return Ok(await _userManagementService.RemoveUserFromRoleAsync(userToRoleDto));
        }

        [Authorize(Roles = "SUPER USER, SALES MANAGER")]
        [HttpGet("GetUserRoles/{userid}")]
        public async Task<ActionResult> GetUserRoles(string userid)
        {
            return Ok(await _userManagementService.GetUserRolesByIdAsync(userid));
        }

        [HttpPost("PasswordReset")]
        public async Task<IActionResult> ResetPassword([FromBody] PasswordResetRequest passwordResetRequest)
        {
            return Ok(await _authenticationService.ResetPassword(passwordResetRequest));
        }

        [Authorize(Roles = "SUPER USER")]
        [HttpGet("GetUsers")]
        public async Task<ActionResult> GetUsers()
        {
            return Ok(await _userManagementService.GetUsers());
        }

        [Authorize(Roles = "SALES MANAGER")]
        [HttpGet("GetUsersByRoleCategory/{username}")]
        public async Task<ActionResult> GetUsersByRoleCategory(string username)
        {
            return Ok(await _userManagementService.GetUsersByManager(username));
        }
    }
}