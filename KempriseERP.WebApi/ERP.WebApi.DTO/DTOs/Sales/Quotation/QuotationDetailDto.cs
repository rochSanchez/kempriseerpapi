﻿namespace ERP.WebApi.DTO.DTOs.Sales.Quotation
{
    public class QuotationDetailDto
    {
        public int QuotationDetailId { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public decimal Total { get; set; }

        public string PartNumber { get; set; }

        public int QuotationId { get; set; }

        public int? PartId { get; set; }
    }
}