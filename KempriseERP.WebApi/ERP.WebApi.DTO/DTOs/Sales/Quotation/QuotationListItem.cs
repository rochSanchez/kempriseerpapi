﻿using System;

namespace ERP.WebApi.DTO.DTOs.Sales.Quotation
{
    public class QuotationListItem : EntityListItem
    {
        public int QuotationId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string QuotationNumber { get; set; }

        public decimal Version { get; set; }

        public string ProjectName { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Status { get; set; }

        public decimal Total { get; set; }

        public string CustomerName { get; set; }

        public string Company { get; set; }

        public string CurrencyCode { get; set; }

        public int? Sequence { get; set; }

        public string CreatedByFullName { get; set; }

        public string UpdatedByFullName { get; set; }

        public override string ToString()
        {
            return ProjectName;
        }
    }
}