﻿using ERP.WebApi.DTO.DTOs.Independent;
using System.Collections.Generic;

namespace ERP.WebApi.DTO.DTOs.Sales.Quotation
{
    public class QuotationAggregate
    {
        public QuotationAggregate()
        {
            QuotationDto = new QuotationDto();
            QuotationDetails = new List<QuotationDetailDto>();
            QuotationNotes = new List<NoteDto>();
            QuotationFiles = new List<FileDto>();
        }

        public QuotationDto QuotationDto { get; set; }

        public ICollection<QuotationDetailDto> QuotationDetails { get; set; }

        public ICollection<NoteDto> QuotationNotes { get; set; }

        public ICollection<FileDto> QuotationFiles { get; set; }
    }
}