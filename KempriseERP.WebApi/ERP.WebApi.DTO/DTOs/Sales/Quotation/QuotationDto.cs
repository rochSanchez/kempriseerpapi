﻿using System;

namespace ERP.WebApi.DTO.DTOs.Sales.Quotation
{
    public class QuotationDto : AuditedDto
    {
        public int QuotationId { get; set; }

        public int InquiryId { get; set; }

        public string QuotationNumber { get; set; }

        public decimal Version { get; set; }

        public string ProjectName { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string PaymentTerms { get; set; }

        public string Status { get; set; }

        public decimal SubTotal { get; set; }

        public decimal DiscountAmount { get; set; }

        public decimal DiscountPercentage { get; set; }

        public decimal TotalDiscount { get; set; }

        public decimal TaxPercentage { get; set; }

        public decimal TaxAmount { get; set; }

        public decimal Total { get; set; }

        public string ReasonForClosing { get; set; }

        public string DetailedReasonForClosing { get; set; }

        // CUSTOMER DETAILS

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        // COMPANY DETAILS

        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }

        // CURRENCY DETAILS

        public int CurrencyId { get; set; }

        public string CurrencyName { get; set; }

        // CURRENCY DETAILS

        public string SalesPersonId { get; set; }

        public string SalesPersonName { get; set; }
    }
}