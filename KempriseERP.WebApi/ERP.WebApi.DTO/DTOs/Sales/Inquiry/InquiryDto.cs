﻿using System;

namespace ERP.WebApi.DTO.DTOs.Sales.Inquiry
{
    public class InquiryDto : AuditedDto
    {
        public int InquiryId { get; set; }

        public string InquiryNumber { get; set; }

        public string ProjectName { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string PaymentTerms { get; set; }

        public string Status { get; set; }

        public string ReasonForClosing { get; set; }

        public string DetailedReasonForClosing { get; set; }

        // CUSTOMER DETAILS

        public int CustomerId { get; set; }

        public string CustomerName { get; set; }

        // COMPANY DETAILS

        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }

        // CURRENCY DETAILS

        public int CurrencyId { get; set; }

        public string CurrencyName { get; set; }

        // SALES PERSON DETAILS

        public string SalesPersonId { get; set; }

        public string SalesPersonName { get; set; }
    }
}