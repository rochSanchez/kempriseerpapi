﻿using System;

namespace ERP.WebApi.DTO.DTOs.Sales.Inquiry
{
    public class InquiryListItem : EntityListItem
    {
        public int InquiryId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string InquiryNumber { get; set; }

        public string ProjectName { get; set; }

        public DateTime? DateCreated { get; set; }

        public string CustomerName { get; set; }

        public string Company { get; set; }

        public string CurrencyCode { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Status { get; set; }

        public int? Sequence { get; set; }

        public string CreatedByFullName { get; set; }

        public string UpdatedByFullName { get; set; }

        public override string ToString()
        {
            return ProjectName;
        }
    }
}