﻿namespace ERP.WebApi.DTO.DTOs.Sales.Inquiry
{
    public class InquiryDetailDto
    {
        public int InquiryDetailId { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public decimal Quantity { get; set; }

        public string PartNumber { get; set; }

        public decimal Price { get; set; }

        public int InquiryId { get; set; }

        public int? PartId { get; set; }
    }
}