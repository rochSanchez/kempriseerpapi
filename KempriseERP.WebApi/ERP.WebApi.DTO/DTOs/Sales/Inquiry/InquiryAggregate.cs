﻿using System.Collections.Generic;
using ERP.WebApi.DTO.DTOs.Independent;

namespace ERP.WebApi.DTO.DTOs.Sales.Inquiry
{
    public class InquiryAggregate
    {
        public InquiryAggregate()
        {
            InquiryDto = new InquiryDto();
            InquiryDetails = new List<InquiryDetailDto>();
            InquiryFiles = new List<FileDto>();
        }

        public InquiryDto InquiryDto { get; set; }

        public ICollection<InquiryDetailDto> InquiryDetails { get; set; }

        public ICollection<FileDto> InquiryFiles { get; set; }
    }
}