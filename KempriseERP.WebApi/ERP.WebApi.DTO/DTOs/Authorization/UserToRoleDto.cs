﻿namespace ERP.WebApi.DTO.DTOs.Authorization
{
    public class UserToRoleDto
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}