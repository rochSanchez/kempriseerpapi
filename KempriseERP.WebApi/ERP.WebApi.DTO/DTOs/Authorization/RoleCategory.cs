﻿namespace ERP.WebApi.DTO.DTOs.Authorization
{
    public enum RoleCategory
    {
        Sales = 1,
        Procurement
    }
}