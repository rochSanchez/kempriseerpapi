﻿namespace ERP.WebApi.DTO.DTOs.Authorization
{
    public class UserListItem : EntityListItem
    {
        public string FullName { get; set; }

        public override string ToString()
        {
            return FullName;
        }
    }
}