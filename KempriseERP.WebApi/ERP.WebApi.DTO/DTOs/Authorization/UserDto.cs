﻿namespace ERP.WebApi.DTO.DTOs.Authorization
{
    public class UserDto : EntityListItem
    {
        public string UserId
        {
            get => Id;
            set => Id = value;
        }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public byte[] UserPhoto { get; set; }

        public override string ToString()
        {
            return FullName;
        }
    }
}