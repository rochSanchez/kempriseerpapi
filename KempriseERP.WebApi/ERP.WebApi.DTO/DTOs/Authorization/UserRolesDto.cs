﻿using System.Collections.Generic;

namespace ERP.WebApi.DTO.DTOs.Authorization
{
    public class UserRolesDto
    {
        public List<UserRole> UserRoles { get; set; }
        public List<UserRole> Roles { get; set; }
        public UserDto User { get; set; }
    }

    public class UserRole
    {
        public string Name { get; set; }
        public RoleCategory RoleCategory { get; set; }
    }
}