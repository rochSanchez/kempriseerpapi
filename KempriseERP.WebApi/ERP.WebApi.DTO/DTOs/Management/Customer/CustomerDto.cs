﻿namespace ERP.WebApi.DTO.DTOs.Management.Customer
{
    public class CustomerDto
    {
        public int CustomerId { get; set; }

        public string CustomerNumber { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string MobileNumber { get; set; }

        public string TaxId { get; set; }

        public string Email { get; set; }

        public string Comments { get; set; }

        // COMPANY DETAILS

        public int? CompanyId { get; set; }

        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }

        public string OfficeNumber { get; set; }

        public string Website { get; set; }
    }
}