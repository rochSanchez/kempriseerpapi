﻿using System;

namespace ERP.WebApi.DTO.DTOs.Management.Customer
{
    public class CustomerListItem : EntityListItem
    {
        public int CustomerId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string CustomerNumber { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string MobileNumber { get; set; }

        public string Email { get; set; }

        // COMPANY DETAILS

        public string CompanyName { get; set; }

        public string OfficeNumber { get; set; }

        public string Website { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}