﻿using System;

namespace ERP.WebApi.DTO.DTOs.Management.Company
{
    public class CompanyListItem : EntityListItem
    {
        public int CompanyId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string Name { get; set; }

        public string OfficeNumber { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}