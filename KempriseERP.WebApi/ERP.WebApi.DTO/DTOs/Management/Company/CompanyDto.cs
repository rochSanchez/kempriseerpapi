﻿namespace ERP.WebApi.DTO.DTOs.Management.Company
{
    public class CompanyDto
    {
        public int CompanyId { get; set; }

        public string Name { get; set; }

        public string CompanyAddress { get; set; }

        public string OfficeNumber { get; set; }

        public string Email { get; set; }

        public string Website { get; set; }

        public string Comments { get; set; }
    }
}