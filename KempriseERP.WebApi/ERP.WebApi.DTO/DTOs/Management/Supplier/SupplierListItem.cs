﻿using System;

namespace ERP.WebApi.DTO.DTOs.Management.Supplier
{
    public class SupplierListItem : EntityListItem
    {
        public int SupplierId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string SupplierNumber { get; set; }

        public string Name { get; set; }

        public string SupplierAddress { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}