﻿namespace ERP.WebApi.DTO.DTOs.Management.Supplier
{
    public class SupplierDto
    {
        public int SupplierId { get; set; }

        public string SupplierNumber { get; set; }

        public string Name { get; set; }

        public string SupplierAddress { get; set; }

        public string Comments { get; set; }

        public int? CompanyId { get; set; }
    }
}