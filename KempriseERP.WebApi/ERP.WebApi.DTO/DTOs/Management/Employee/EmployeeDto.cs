﻿namespace ERP.WebApi.DTO.DTOs.Management.Employee
{
    public class EmployeeDto
    {
        public int EmployeeId { get; set; }

        public string EmployeeNumber { get; set; }

        public string Name { get; set; }

        public string Comments { get; set; }
    }
}