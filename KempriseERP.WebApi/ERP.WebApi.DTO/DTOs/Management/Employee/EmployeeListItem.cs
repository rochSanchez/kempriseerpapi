﻿using System;

namespace ERP.WebApi.DTO.DTOs.Management.Employee
{
    public class EmployeeListItem : EntityListItem
    {
        public int EmployeeId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string EmployeeNumber { get; set; }

        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}