﻿using System.Collections.Generic;
using ERP.WebApi.DTO.DTOs.Independent;

namespace ERP.WebApi.DTO.DTOs.Management.Employee
{
    public class EmployeeAggregate
    {
        public EmployeeAggregate()
        {
            EmployeeDto = new EmployeeDto();
            EmployeeFiles = new List<FileDto>();
        }

        public EmployeeDto EmployeeDto { get; set; }

        public ICollection<FileDto> EmployeeFiles { get; set; }
    }
}