﻿using System;

namespace ERP.WebApi.DTO.DTOs.Management.Currency
{
    public class CurrencyListItem : EntityListItem
    {
        public int CurrencyId
        {
            get => Convert.ToInt32(Id);
            set => Id = value.ToString();
        }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Comments { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}