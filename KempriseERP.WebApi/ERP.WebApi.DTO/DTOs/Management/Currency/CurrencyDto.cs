﻿namespace ERP.WebApi.DTO.DTOs.Management.Currency
{
    public class CurrencyDto
    {
        public int CurrencyId { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Comments { get; set; }
    }
}