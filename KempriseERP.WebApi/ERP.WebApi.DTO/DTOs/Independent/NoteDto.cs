﻿namespace ERP.WebApi.DTO.DTOs.Independent
{
    public class NoteDto
    {
        public int NoteId { get; set; }

        public decimal Item { get; set; }

        public string Name { get; set; }
    }
}