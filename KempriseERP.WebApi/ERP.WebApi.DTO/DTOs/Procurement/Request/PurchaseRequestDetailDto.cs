﻿namespace ERP.WebApi.DTO.DTOs.Procurement.Request
{
    public class PurchaseRequestDetailDto
    {
        public int PurchaseRequestDetailId { get; set; }

        public int PurchaseRequestId { get; set; }

        public string Description { get; set; }

        public int ProductId { get; set; }

        public string Category { get; set; }

        public decimal Quantity { get; set; }

        public decimal UnitPrice { get; set; }
    }
}