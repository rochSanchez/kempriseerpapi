﻿using System;

namespace ERP.WebApi.DTO.DTOs.Procurement.Request
{
    public class PurchaseRequestDto : AuditedDto
    {
        public int PurchaseRequestId { get; set; }

        public int SupplierId { get; set; }

        public int CurrencyId { get; set; }

        public string PurchaseRepresentativeId { get; set; }

        public string PurchaseRequestNumber { get; set; }

        public string ProjectName { get; set; }

        public DateTime? DateCreated { get; set; }

        public string Status { get; set; }

        public string ReasonForClosing { get; set; }
    }
}