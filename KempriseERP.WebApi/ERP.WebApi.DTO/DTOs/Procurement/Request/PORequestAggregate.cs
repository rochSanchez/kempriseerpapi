﻿using System.Collections.Generic;
using ERP.WebApi.DTO.DTOs.Independent;

namespace ERP.WebApi.DTO.DTOs.Procurement.Request
{
    public class PoRequestAggregate
    {
        public PoRequestAggregate()
        {
            PurchaseRequestDto = new PurchaseRequestDto();
            PurchaseRequestDetails = new List<PurchaseRequestDetailDto>();
            PORequestFiles = new List<FileDto>();
        }

        public PurchaseRequestDto PurchaseRequestDto { get; set; }

        public ICollection<PurchaseRequestDetailDto> PurchaseRequestDetails { get; set; }

        public ICollection<FileDto> PORequestFiles { get; set; }
    }
}