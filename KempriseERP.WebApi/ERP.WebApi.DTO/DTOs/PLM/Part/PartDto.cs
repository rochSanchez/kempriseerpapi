﻿namespace ERP.WebApi.DTO.DTOs.PLM.Part
{
    public class PartDto
    {
        public int PartId { get; set; }

        public string PartNumber { get; set; }

        public string Description { get; set; }

        public string Category { get; set; }

        public decimal Price { get; set; }

        public int CategoryId { get; set; }
    }
}