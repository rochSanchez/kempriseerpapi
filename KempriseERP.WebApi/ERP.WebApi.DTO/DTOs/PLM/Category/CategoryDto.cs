﻿namespace ERP.WebApi.DTO.DTOs.PLM.Category
{
    public class CategoryDto
    {
        public int PartId { get; set; }

        public string PartNumber { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public int CategoryId { get; set; }
    }
}