﻿namespace ERP.WebApi.DTO.DTOs.Files
{
    public class FileDto
    {
        public int FileId { get; set; }

        public string FileName { get; set; }

        public string FileFormat { get; set; }

        public byte[] FileData { get; set; }
    }
}