﻿namespace ERP.WebApi.DTO.Constants
{
    public class InquiryStatus
    {
        public const string PendingReview = "PENDING REVIEW";
        public const string QuotationCreated = "QUOTATION CREATED";
        public const string Closed = "CLOSED";
    }

    public class QuotationStatus
    {
        public const string SubmittedForChecking = "SUBMITTED FOR CHECKING";
        public const string RevisionRequested = "REVISION REQUESTED";
        public const string Closed = "CLOSED";
        public const string SubmittedForApproval = "SUBMITTED FOR APPROVAL";
        public const string Approved = "APPROVED";
        public const string QuotationSent = "QUOTATION SENT";
        public const string SalesOrder = "SALES ORDER";
    }
}