﻿namespace ERP.WebApi.DTO.Constants
{
    public static class ErpRoles
    {
        public static string SuperUser = "SUPER USER";

        public static string[] CanEdit = { "SUPER USER", "MANAGER" };

        public static string[] SalesRoles = { "SUPER USER", "SALES ADMIN", "SALES ENGINEER", "SENIOR SALES", "SALES MANAGER" };
        public static string[] CreateQuotation = { "SUPER USER", "SALES ENGINEER", "SENIOR SALES", "SALES MANAGER" };
        public static string[] CheckQuotation = { "SUPER USER", "SENIOR SALES", "SALES MANAGER" };
        public static string[] ApproveQuotation = { "SUPER USER", "SALES MANAGER" };
    }
}