﻿using ERP.WebApi.DTO.Security.Authentication;
using System.Threading.Tasks;

namespace ERP.WebApi.DTO.Security.Contracts
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResponse> AuthenticateAsync(AuthenticationRequest request);

        Task<RegistrationResponse> RegisterAsync(RegistrationRequest request);

        Task<bool> ResetPassword(PasswordResetRequest passwordReset);
    }
}