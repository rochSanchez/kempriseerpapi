﻿using ERP.WebApi.DTO.DTOs.Authorization;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.DTO.Security.Contracts
{
    public interface IUserManagementService
    {
        Task<string> CreateRoleAsync(string roleName);

        Task<UserRolesDto> AddUserToRoleAsync(UserToRoleDto addUserToRoleDto);

        Task<string> RemoveUserFromRoleAsync(UserToRoleDto addUserToRoleDto);

        Task<UserRolesDto> GetUserRolesAsync(string userName);

        Task<UserRolesDto> GetUserRolesByIdAsync(string userId);

        Task<List<UserDto>> GetUsers();

        Task<List<UserDto>> GetUserByRoleCategory(RoleCategory roleCategory);

        Task<List<UserDto>> GetUsersByManager(string username);

        Task<UserDto> GetUserByIdAsync(string userId);

        Task<List<UserDto>> GetConcernedUsers(string[] roleNames);
    }
}