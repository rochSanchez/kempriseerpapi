﻿using System.ComponentModel.DataAnnotations;

namespace ERP.WebApi.DTO.Security.UserManagement
{
    public class AddUserToRoleRequest
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Role { get; set; }
    }
}