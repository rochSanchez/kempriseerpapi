﻿namespace ERP.WebApi.DTO.Security.Authentication
{
    public class PasswordResetRequest
    {
        public string UserName { get; set; }

        public string NewPassword { get; set; }
    }
}