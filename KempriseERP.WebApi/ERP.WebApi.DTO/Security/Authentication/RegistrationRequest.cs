﻿namespace ERP.WebApi.DTO.Security.Authentication
{
    public class RegistrationRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string UserName => $"{FirstName}.{LastName}";

        public string Password { get; set; }

        public byte[] UserPhoto { get; set; }
    }
}