﻿using System.ComponentModel.DataAnnotations;

namespace ERP.WebApi.DTO.Security.Authentication
{
    public class AddRoleRequest
    {
        [Required]
        public string RoleName { get; set; }
    }
}