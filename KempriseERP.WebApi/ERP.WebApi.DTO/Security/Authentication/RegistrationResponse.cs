﻿namespace ERP.WebApi.DTO.Security.Authentication
{
    public class RegistrationResponse
    {
        public string UserId { get; set; }
    }
}