﻿namespace ERP.WebApi.DTO.Lookups
{
    public class LookupEntity
    {
        public virtual string Id { get; set; }
        public virtual string Text { get; set; }
        public bool IsSelected { get; set; }
    }
}