﻿namespace ERP.WebApi.DTO.Lookups.LookupItems
{
    public class SalesPersonLookup : LookupEntity
    {
        public override string Id { get; set; }

        public override string Text { get; set; }
    }
}