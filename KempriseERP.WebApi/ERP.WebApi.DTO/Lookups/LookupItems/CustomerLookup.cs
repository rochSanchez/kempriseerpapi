﻿namespace ERP.WebApi.DTO.Lookups.LookupItems
{
    public class CustomerLookup : LookupEntity
    {
        public override string Id { get; set; }

        public override string Text { get; set; }

        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }
    }
}