﻿namespace ERP.WebApi.DTO.Lookups.LookupItems
{
    public class ReasonLookup : LookupEntity
    {
        public override string Id { get; set; }

        public override string Text { get; set; }
    }
}