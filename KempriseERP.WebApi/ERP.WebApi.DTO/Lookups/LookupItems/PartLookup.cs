﻿namespace ERP.WebApi.DTO.Lookups.LookupItems
{
    public class PartLookup : LookupEntity
    {
        public override string Id { get; set; }

        public override string Text { get; set; }

        public string PartNumber { get; set; }

        public decimal Price { get; set; }
    }
}