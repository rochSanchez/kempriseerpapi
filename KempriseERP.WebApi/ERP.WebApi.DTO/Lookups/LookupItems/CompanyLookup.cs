﻿namespace ERP.WebApi.DTO.Lookups.LookupItems
{
    public class CompanyLookup : LookupEntity
    {
        public override string Id { get; set; }

        public override string Text { get; set; }

        public string CompanyAddress { get; set; }

        public string OfficeNumber { get; set; }

        public string Website { get; set; }
    }
}