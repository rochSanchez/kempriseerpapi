﻿using ERP.WebApi.DTO.Lookups.LookupItems;
using System.Collections.Generic;

namespace ERP.WebApi.DTO.Lookups.LookupAggregates
{
    public class SalesLookupAggregate : Lookups
    {
        public SalesLookupAggregate()
        {
            Customers = new List<CustomerLookup>();
            Currencies = new List<CurrencyLookup>();
            Reasons = new List<ReasonLookup>();
            Categories = new List<CategoryLookup>();
            Parts = new List<PartLookup>();
            SalesPersons = new List<SalesPersonLookup>();
        }

        public List<SalesPersonLookup> SalesPersons { get; set; }

        public List<CustomerLookup> Customers { get; set; }

        public List<CurrencyLookup> Currencies { get; set; }

        public List<ReasonLookup> Reasons { get; set; }

        public List<CategoryLookup> Categories { get; set; }

        public List<PartLookup> Parts { get; set; }
    }
}