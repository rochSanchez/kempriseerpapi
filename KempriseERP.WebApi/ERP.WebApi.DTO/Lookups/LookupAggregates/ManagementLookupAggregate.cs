﻿using ERP.WebApi.DTO.Lookups.LookupItems;
using System.Collections.Generic;

namespace ERP.WebApi.DTO.Lookups.LookupAggregates
{
    public class ManagementLookupAggregate : Lookups
    {
        public ManagementLookupAggregate()
        {
            Companies = new List<CompanyLookup>();
        }

        public List<CompanyLookup> Companies { get; set; }
    }
}