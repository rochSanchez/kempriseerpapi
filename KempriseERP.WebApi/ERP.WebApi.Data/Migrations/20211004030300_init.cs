﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace ERP.WebApi.Data.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    category_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    categoryname = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.category_id);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    company_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    companyaddress = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    officenumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    website = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    comments = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.company_id);
                });

            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    currency_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    code = table.Column<string>(type: "varchar(20)", maxLength: 20, nullable: true),
                    comments = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.currency_id);
                });

            migrationBuilder.CreateTable(
                name: "Employee",
                columns: table => new
                {
                    employee_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    employeenumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    comments = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employee", x => x.employee_id);
                });

            migrationBuilder.CreateTable(
                name: "Part",
                columns: table => new
                {
                    part_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    partnumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    description = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    category = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    price = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Part", x => x.part_id);
                });

            migrationBuilder.CreateTable(
                name: "PartAttribute",
                columns: table => new
                {
                    partattribute_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartAttribute", x => x.partattribute_id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    product_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    productyype_id = table.Column<int>(type: "int", nullable: false),
                    tax_id = table.Column<int>(type: "int", nullable: false),
                    supplier_id = table.Column<int>(type: "int", nullable: false),
                    productname = table.Column<string>(type: "text", nullable: true),
                    partnumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    description = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    partcategory = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    price = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    canbesold = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    canbepurchased = table.Column<bool>(type: "tinyint(1)", nullable: false),
                    requirespartnumber = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.product_id);
                });

            migrationBuilder.CreateTable(
                name: "ProductPartAttributes",
                columns: table => new
                {
                    productpartattribute_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    partattribute_id = table.Column<int>(type: "int", nullable: false),
                    product_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPartAttributes", x => x.productpartattribute_id);
                });

            migrationBuilder.CreateTable(
                name: "Reason",
                columns: table => new
                {
                    reason_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reason", x => x.reason_id);
                });

            migrationBuilder.CreateTable(
                name: "Tax",
                columns: table => new
                {
                    tax_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    taxname = table.Column<string>(type: "text", nullable: true),
                    taxscope = table.Column<int>(type: "int", nullable: false),
                    amount = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    isactive = table.Column<bool>(type: "tinyint(1)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tax", x => x.tax_id);
                });

            migrationBuilder.CreateTable(
                name: "Customer",
                columns: table => new
                {
                    customer_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    customernumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    company_id = table.Column<int>(type: "int", nullable: true),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    designation = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    mobilenumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    taxid = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    email = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    comments = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer", x => x.customer_id);
                    table.ForeignKey(
                        name: "FK_Customer_Companies_company_id",
                        column: x => x.company_id,
                        principalTable: "Companies",
                        principalColumn: "company_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Supplier",
                columns: table => new
                {
                    supplier_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    suppliernumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    name = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    supplieraddress = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    comments = table.Column<string>(type: "varchar(300)", maxLength: 300, nullable: true),
                    company_id = table.Column<int>(type: "int", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplier", x => x.supplier_id);
                    table.ForeignKey(
                        name: "FK_Supplier_Companies_company_id",
                        column: x => x.company_id,
                        principalTable: "Companies",
                        principalColumn: "company_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseRequisitionDetail",
                columns: table => new
                {
                    purchaserequisitiondetail_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    description = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    product_id = table.Column<int>(type: "int", nullable: false),
                    category = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    quantity = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    unitprice = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseRequisitionDetail", x => x.purchaserequisitiondetail_id);
                    table.ForeignKey(
                        name: "FK_PurchaseRequisitionDetail_Product_product_id",
                        column: x => x.product_id,
                        principalTable: "Product",
                        principalColumn: "product_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Inquiry",
                columns: table => new
                {
                    inquiry_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    inquirynumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    projectname = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    datecreated = table.Column<DateTime>(type: "datetime", nullable: true),
                    expirationdate = table.Column<DateTime>(type: "datetime", nullable: true),
                    paymentterms = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    status = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    reasonforclosing = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    detailedreasonforclosing = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    customer_id = table.Column<int>(type: "int", nullable: false),
                    currencyid = table.Column<int>(type: "int", nullable: false),
                    salespersonid = table.Column<string>(type: "text", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inquiry", x => x.inquiry_id);
                    table.ForeignKey(
                        name: "FK_Inquiry_Currency_currencyid",
                        column: x => x.currencyid,
                        principalTable: "Currency",
                        principalColumn: "currency_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Inquiry_Customer_customer_id",
                        column: x => x.customer_id,
                        principalTable: "Customer",
                        principalColumn: "customer_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Quotation",
                columns: table => new
                {
                    quotation_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    inquiry_id = table.Column<int>(type: "int", nullable: false),
                    quotationnumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    version = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    projectname = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    datecreated = table.Column<DateTime>(type: "datetime", nullable: true),
                    expirationdate = table.Column<DateTime>(type: "datetime", nullable: true),
                    paymentterms = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    status = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    subtotal = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    discountamount = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    discountpercentage = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    totaldiscount = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    taxpercentage = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    taxamount = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    total = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    reasonforclosing = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    detailedreasonforclosing = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    customer_id = table.Column<int>(type: "int", nullable: false),
                    currencyid = table.Column<int>(type: "int", nullable: false),
                    salespersonid = table.Column<string>(type: "text", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quotation", x => x.quotation_id);
                    table.ForeignKey(
                        name: "FK_Quotation_Currency_currencyid",
                        column: x => x.currencyid,
                        principalTable: "Currency",
                        principalColumn: "currency_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Quotation_Customer_customer_id",
                        column: x => x.customer_id,
                        principalTable: "Customer",
                        principalColumn: "customer_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseRequisition",
                columns: table => new
                {
                    purchaserequisition_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    supplier_id = table.Column<int>(type: "int", nullable: false),
                    currencyid = table.Column<int>(type: "int", nullable: false),
                    purchaserepresentative_id = table.Column<string>(type: "text", nullable: true),
                    purchaserequisitionnumber = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    projectname = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    datecreated = table.Column<DateTime>(type: "datetime", nullable: true),
                    status = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    reasonforclosing = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseRequisition", x => x.purchaserequisition_id);
                    table.ForeignKey(
                        name: "FK_PurchaseRequisition_Currency_currencyid",
                        column: x => x.currencyid,
                        principalTable: "Currency",
                        principalColumn: "currency_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PurchaseRequisition_Supplier_supplier_id",
                        column: x => x.supplier_id,
                        principalTable: "Supplier",
                        principalColumn: "supplier_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InquiryDetail",
                columns: table => new
                {
                    inquirydetail_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    description = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    category = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    quantity = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    inquiry_id = table.Column<int>(type: "int", nullable: false),
                    part_id = table.Column<int>(type: "int", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InquiryDetail", x => x.inquirydetail_id);
                    table.ForeignKey(
                        name: "FK_InquiryDetail_Inquiry_inquiry_id",
                        column: x => x.inquiry_id,
                        principalTable: "Inquiry",
                        principalColumn: "inquiry_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InquiryDetail_Part_part_id",
                        column: x => x.part_id,
                        principalTable: "Part",
                        principalColumn: "part_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "File",
                columns: table => new
                {
                    file_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    filename = table.Column<string>(type: "varchar(100)", maxLength: 100, nullable: true),
                    fileformat = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    filedata = table.Column<byte[]>(type: "varbinary(4000)", nullable: true),
                    EmployeeId = table.Column<int>(type: "int", nullable: true),
                    InquiryId = table.Column<int>(type: "int", nullable: true),
                    QuotationId = table.Column<int>(type: "int", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_File", x => x.file_id);
                    table.ForeignKey(
                        name: "FK_File_Employee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employee",
                        principalColumn: "employee_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_Inquiry_InquiryId",
                        column: x => x.InquiryId,
                        principalTable: "Inquiry",
                        principalColumn: "inquiry_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_File_Quotation_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotation",
                        principalColumn: "quotation_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Note",
                columns: table => new
                {
                    note_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    item = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    name = table.Column<string>(type: "varchar(500)", maxLength: 500, nullable: true),
                    QuotationId = table.Column<int>(type: "int", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Note", x => x.note_id);
                    table.ForeignKey(
                        name: "FK_Note_Quotation_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotation",
                        principalColumn: "quotation_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuotationDetail",
                columns: table => new
                {
                    quotationdetail_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    description = table.Column<string>(type: "varchar(200)", maxLength: 200, nullable: true),
                    category = table.Column<string>(type: "varchar(50)", maxLength: 50, nullable: true),
                    quantity = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    price = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    total = table.Column<decimal>(type: "decimal(18, 2)", precision: 14, scale: 2, nullable: false),
                    quotation_id = table.Column<int>(type: "int", nullable: false),
                    part_id = table.Column<int>(type: "int", nullable: true),
                    Sequence = table.Column<int>(type: "int", nullable: true),
                    UpdatedAt = table.Column<DateTime>(type: "datetime", nullable: true),
                    UpdatedById = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByLastName = table.Column<string>(type: "text", nullable: true),
                    UpdatedByFullName = table.Column<string>(type: "text", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedById = table.Column<string>(type: "text", nullable: true),
                    CreatedByFirstName = table.Column<string>(type: "text", nullable: true),
                    CreatedByLastName = table.Column<string>(type: "text", nullable: true),
                    CreatedByFullName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationDetail", x => x.quotationdetail_id);
                    table.ForeignKey(
                        name: "FK_QuotationDetail_Part_part_id",
                        column: x => x.part_id,
                        principalTable: "Part",
                        principalColumn: "part_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuotationDetail_Quotation_quotation_id",
                        column: x => x.quotation_id,
                        principalTable: "Quotation",
                        principalColumn: "quotation_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Currency",
                columns: new[] { "currency_id", "code", "comments", "CreatedAt", "CreatedByFirstName", "CreatedByFullName", "CreatedById", "CreatedByLastName", "name", "Sequence", "UpdatedAt", "UpdatedByFirstName", "UpdatedByFullName", "UpdatedById", "UpdatedByLastName" },
                values: new object[,]
                {
                    { 1, "USD", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, null, null, "UNITED STATES DOLLAR", 1, new DateTime(2021, 10, 4, 11, 2, 59, 992, DateTimeKind.Local).AddTicks(7925), null, null, null, null },
                    { 2, "AED", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, null, null, "UNITED ARAB EMIRATES DIRHAM", 2, new DateTime(2021, 10, 4, 11, 2, 59, 993, DateTimeKind.Local).AddTicks(8010), null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "Reason",
                columns: new[] { "reason_id", "CreatedAt", "CreatedByFirstName", "CreatedByFullName", "CreatedById", "CreatedByLastName", "name", "Sequence", "UpdatedAt", "UpdatedByFirstName", "UpdatedByFullName", "UpdatedById", "UpdatedByLastName" },
                values: new object[,]
                {
                    { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, null, null, "INFEASIBLE", null, null, null, null, null, null },
                    { 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, null, null, "LOST BIDDING", null, null, null, null, null, null },
                    { 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, null, null, "REVISED", null, null, null, null, null, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Category_categoryname",
                table: "Category",
                column: "categoryname",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Companies_name",
                table: "Companies",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Currency_code",
                table: "Currency",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Currency_name",
                table: "Currency",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customer_company_id",
                table: "Customer",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_customernumber",
                table: "Customer",
                column: "customernumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customer_name",
                table: "Customer",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee_employeenumber",
                table: "Employee",
                column: "employeenumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Employee_name",
                table: "Employee",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_File_EmployeeId",
                table: "File",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_File_InquiryId",
                table: "File",
                column: "InquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_File_QuotationId",
                table: "File",
                column: "QuotationId");

            migrationBuilder.CreateIndex(
                name: "IX_Inquiry_currencyid",
                table: "Inquiry",
                column: "currencyid");

            migrationBuilder.CreateIndex(
                name: "IX_Inquiry_customer_id",
                table: "Inquiry",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_Inquiry_inquirynumber",
                table: "Inquiry",
                column: "inquirynumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_InquiryDetail_inquiry_id",
                table: "InquiryDetail",
                column: "inquiry_id");

            migrationBuilder.CreateIndex(
                name: "IX_InquiryDetail_part_id",
                table: "InquiryDetail",
                column: "part_id");

            migrationBuilder.CreateIndex(
                name: "IX_Note_QuotationId",
                table: "Note",
                column: "QuotationId");

            migrationBuilder.CreateIndex(
                name: "IX_Part_partnumber",
                table: "Part",
                column: "partnumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseRequisition_currencyid",
                table: "PurchaseRequisition",
                column: "currencyid");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseRequisition_supplier_id",
                table: "PurchaseRequisition",
                column: "supplier_id");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseRequisitionDetail_product_id",
                table: "PurchaseRequisitionDetail",
                column: "product_id");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_currencyid",
                table: "Quotation",
                column: "currencyid");

            migrationBuilder.CreateIndex(
                name: "IX_Quotation_customer_id",
                table: "Quotation",
                column: "customer_id");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDetail_part_id",
                table: "QuotationDetail",
                column: "part_id");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationDetail_quotation_id",
                table: "QuotationDetail",
                column: "quotation_id");

            migrationBuilder.CreateIndex(
                name: "IX_Reason_name",
                table: "Reason",
                column: "name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Supplier_company_id",
                table: "Supplier",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "IX_Supplier_suppliernumber",
                table: "Supplier",
                column: "suppliernumber",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "File");

            migrationBuilder.DropTable(
                name: "InquiryDetail");

            migrationBuilder.DropTable(
                name: "Note");

            migrationBuilder.DropTable(
                name: "PartAttribute");

            migrationBuilder.DropTable(
                name: "ProductPartAttributes");

            migrationBuilder.DropTable(
                name: "PurchaseRequisition");

            migrationBuilder.DropTable(
                name: "PurchaseRequisitionDetail");

            migrationBuilder.DropTable(
                name: "QuotationDetail");

            migrationBuilder.DropTable(
                name: "Reason");

            migrationBuilder.DropTable(
                name: "Tax");

            migrationBuilder.DropTable(
                name: "Employee");

            migrationBuilder.DropTable(
                name: "Inquiry");

            migrationBuilder.DropTable(
                name: "Supplier");

            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Part");

            migrationBuilder.DropTable(
                name: "Quotation");

            migrationBuilder.DropTable(
                name: "Currency");

            migrationBuilder.DropTable(
                name: "Customer");

            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}
