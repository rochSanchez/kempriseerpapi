﻿using ERP.WebApi.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ERP.WebApi.Data.Persistence
{
    public static class ModelBuilderExtensions
    {
        public static void SetDecimalPrecisionAndScale(this ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(decimal) || p.ClrType == typeof(decimal?)))
            {
                property.SetPrecision(14);
                property.SetScale(2);
            }
        }

        public static void SetUniqueConstraints(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasIndex(p => p.CustomerNumber)
                .IsUnique();
            modelBuilder.Entity<Customer>()
                .HasIndex(p => p.Name)
                .IsUnique();

            modelBuilder.Entity<Employee>()
                .HasIndex(p => p.EmployeeNumber)
                .IsUnique();
            modelBuilder.Entity<Employee>()
                .HasIndex(p => p.Name)
                .IsUnique();

            modelBuilder.Entity<Supplier>()
                .HasIndex(p => p.SupplierNumber)
                .IsUnique();

            modelBuilder.Entity<Currency>()
                .HasIndex(p => p.Name)
                .IsUnique();
            modelBuilder.Entity<Currency>()
                .HasIndex(p => p.Code)
                .IsUnique();

            modelBuilder.Entity<Reason>()
                .HasIndex(p => p.Name)
                .IsUnique();

            modelBuilder.Entity<Inquiry>()
                .HasIndex(p => p.InquiryNumber)
                .IsUnique();

            modelBuilder.Entity<Company>()
                .HasIndex(p => p.Name)
                .IsUnique();

            modelBuilder.Entity<Part>()
                .HasIndex(p => p.PartNumber)
                .IsUnique();

            modelBuilder.Entity<Category>()
                .HasIndex(p => p.CategoryName)
                .IsUnique();
        }

        public static void SeedData(this ModelBuilder modelBuilder)
        {
            // modelBuilder.Entity<Company>().HasData(
            //     new Company
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 1,
            //         CompanyId = 1,
            //         Name = "COMPANY ONE",
            //         CompanyAddress = "COMPANY ONE ADDRESS",
            //         OfficeNumber = "COMPANY ONE OFFICE NUMBER",
            //         Email = "COMPANY ONE EMAIL",
            //         Website = "COMPANY ONE WEBSITE",
            //         Comments = "COMPANY ONE COMMENTS"
            //     }
            // );
            //
            // modelBuilder.Entity<Customer>().HasData(
            //     new Customer
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 1,
            //         CustomerId = 1,
            //         CustomerNumber = "17-1001",
            //         CompanyId = 1,
            //         Name = "CUSTOMER ONE",
            //         Designation = "CUSTOMER ONE DESIGNATION",
            //         MobileNumber = "CUSTOMER ONE MOBILE",
            //         TaxId = "CUSTOMER ONE TAX ID",
            //         Email = "CUSTOMER ONE EMAIL",
            //         Comments = "CUSTOMER ONE COMMENTS"
            //     }
            // );

            modelBuilder.Entity<Currency>().HasData(
                new Currency
                {
                    UpdatedAt = DateTime.Now,
                    Sequence = 1,
                    CurrencyId = 1,
                    Name = "UNITED STATES DOLLAR",
                    Code = "USD"
                },
                new Currency
                {
                    UpdatedAt = DateTime.Now,
                    Sequence = 2,
                    CurrencyId = 2,
                    Name = "UNITED ARAB EMIRATES DIRHAM",
                    Code = "AED"
                }
            );

            modelBuilder.Entity<Reason>().HasData(
                new Reason
                {
                    ReasonId = 1,
                    Name = "INFEASIBLE"
                },
                new Reason
                {
                    ReasonId = 2,
                    Name = "LOST BIDDING"
                },
                new Reason
                {
                    ReasonId = 3,
                    Name = "REVISED"
                }
            );

            // modelBuilder.Entity<Category>().HasData(
            //     new Category
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 1,
            //         CategoryId = 1,
            //         CategoryName = "Lift Subs"
            //     },
            //     new Category
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 2,
            //         CategoryId = 2,
            //         CategoryName = "Bit Subs (Float Sub)"
            //     }
            // );

            // modelBuilder.Entity<Part>().HasData(
            //     new Part
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 1,
            //         PartId = 1,
            //         PartNumber = "0000001",
            //         Description = "7.25 OD x 3.50 ID UGPDS 55 Box x 7.0OD x 2.812 ID NC 50 Pin Lift Sub (With 5⅞'' Body), 51.0 LG",
            //         Price = 2426,
            //         Category = "Lift Subs"
            //     },
            //     new Part
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 2,
            //         PartId = 2,
            //         PartNumber = "0000002",
            //         Description = "7.250 OD x 3.50 ID UGPDS 55 Box x 8.250 OD x 2.812 ID 6⅝ REG Pin Lift Sub (With 5⅞'' Body), 51.0 LG",
            //         Price = 2817,
            //         Category = "Lift Subs"
            //     },
            //     new Part
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 3,
            //         PartId = 3,
            //         PartNumber = "0000003",
            //         Description = "7.250 OD x 3.50 ID UGPDS 55 Box x 9.50 OD x 3.00 ID 7⅝ REG Pin Lift Sub (With 5⅞'' Body), 51.0 LG",
            //         Price = 3344,
            //         Category = "Lift Subs"
            //     },
            //     new Part
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 4,
            //         PartId = 4,
            //         PartNumber = "0000004",
            //         Description = "5.00 OD x 2.438 ID UGDPS 38 Box - 4.375 OD x 1.50 ID 3½'' REG Box with 2F-3R Float Bore, 51.0 LG",
            //         Price = 1459,
            //         Category = "Bit Subs (Float Sub)"
            //     },
            //     new Part
            //     {
            //         UpdatedAt = DateTime.Now,
            //         Sequence = 5,
            //         PartId = 5,
            //         PartNumber = "0000005",
            //         Description = "7.00 OD x 2.812 NC 50 Box x 5.50 OD x 2.25 ID 4½ REG Box with 4R Float Bore, 51.0 LG",
            //         Price = 1601,
            //         Category = "Bit Subs (Float Sub)"
            //     }
            // );
        }
    }
}