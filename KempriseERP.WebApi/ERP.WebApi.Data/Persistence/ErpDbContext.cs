﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Entities.Procurement;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PartAttribute = ERP.WebApi.Data.Entities.PartAttribute;
using Product = ERP.WebApi.Data.Entities.Product;
using ProductPartAttribute = ERP.WebApi.Data.Entities.ProductPartAttribute;

namespace ERP.WebApi.Data.Persistence
{
    public class ErpDbContext : DbContext
    {
        public ErpDbContext(DbContextOptions<ErpDbContext> options, IConfiguration configuration) : base(options)
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }

        public virtual DbSet<Employee> Employees { get; set; }

        public virtual DbSet<Supplier> Suppliers { get; set; }

        public virtual DbSet<Currency> Currencies { get; set; }

        public virtual DbSet<Reason> Reasons { get; set; }

        public virtual DbSet<Inquiry> Inquiries { get; set; }

        public virtual DbSet<InquiryDetail> InquiryDetails { get; set; }

        public virtual DbSet<Quotation> Quotations { get; set; }

        public virtual DbSet<QuotationDetail> QuotationDetails { get; set; }

        public virtual DbSet<Company> Companies { get; set; }

        public virtual DbSet<Part> Parts { get; set; }

        public virtual DbSet<Category> Categories { get; set; }

        //Procurement
        public virtual DbSet<PurchaseRequest> PurchaseRequests { get; set; }

        public virtual DbSet<PurchaseRequestDetail> PurchaseRequestDetails { get; set; }

        //todo: Finalized
        public virtual DbSet<Product> Products { get; set; }

        public virtual DbSet<Tax> Taxes { get; set; }

        public virtual DbSet<ProductPartAttribute> ProductPartAttributes { get; set; }

        public virtual DbSet<PartAttribute> PartAttributes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.SetDecimalPrecisionAndScale();
            modelBuilder.SetUniqueConstraints();
            modelBuilder.SeedData();
        }
    }
}