﻿using ERP.WebApi.Data.Repository;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.WebApi.Data.Persistence
{
    public static class ErpPersistenceRegistration
    {
        public static IServiceCollection AddErpPersistenceService(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("ErpDataConnectionString");

            services.AddDbContext<ErpDbContext>(dbContextOptions =>
                dbContextOptions.UseMySQL(connectionString));

            services.AddScoped<IFileRepository, FileRepository>()
                .AddScoped<ISupplierRepository, SupplierRepository>()
                .AddScoped<ICustomerRepository, CustomerRepository>()
                .AddScoped<ICompanyRepository, CompanyRepository>()
                .AddScoped<IEmployeeRepository, EmployeeRepository>()
                .AddScoped<ICurrencyRepository, CurrencyRepository>()
                .AddScoped<IReasonRepository, ReasonRepository>()
                .AddScoped<IInquiryRepository, InquiryRepository>()
                .AddScoped<IInquiryDetailRepository, InquiryDetailRepository>()
                .AddScoped<IQuotationRepository, QuotationRepository>()
                .AddScoped<IQuotationDetailRepository, QuotationDetailRepository>()
                .AddScoped<ICategoryRepository, CategoryRepository>()
                .AddScoped<IPartRepository, PartRepository>()
                .AddScoped<IUserContext, UserContext>()
                .AddScoped<IPurchaseRequestRepository, PurchaseRequestRepository>();

            return services;
        }
    }
}