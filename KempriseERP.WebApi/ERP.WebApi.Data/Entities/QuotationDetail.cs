﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("QuotationDetail")]
    public class QuotationDetail : AuditedEntity
    {
        [Column("quotationdetail_id")] [Key] public int QuotationDetailId { get; set; }

        [Column("description")] [MaxLength(200)] public string Description { get; set; }

        [Column("category")] [MaxLength(50)] public string Category { get; set; }

        [Column("quantity")] public decimal Quantity { get; set; }

        [Column("price")] public decimal Price { get; set; }

        [Column("total")] public decimal Total { get; set; }

        [ForeignKey("Quotation")]
        [Column("quotation_id")] public int QuotationId { get; set; }

        [ForeignKey("Part")]
        [Column("part_id")] public int? PartId { get; set; }

        public virtual Quotation Quotation { get; set; }

        public virtual Part Part { get; set; }
    }
}