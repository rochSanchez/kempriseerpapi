﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("File")]
    public class File : AuditedEntity
    {
        [Column("file_id")] [Key] public int FileId { get; set; }

        [Column("filename")] [MaxLength(100)] public string FileName { get; set; }

        [Column("fileformat")] [MaxLength(10)] public string FileFormat { get; set; }

        [Column("filedata")] public byte[] FileData { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Inquiry Inquiry { get; set; }

        public virtual Quotation Quotation { get; set; }
    }
}