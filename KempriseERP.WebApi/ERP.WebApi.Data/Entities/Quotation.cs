﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Quotation")]
    public class Quotation : AuditedEntity
    {
        [Column("quotation_id")] [Key] public int QuotationId { get; set; }

        [Column("inquiry_id")] public int InquiryId { get; set; }

        [Column("quotationnumber")] [MaxLength(50)] public string QuotationNumber { get; set; }

        [Column("version")] public decimal Version { get; set; }

        [Column("projectname")] [MaxLength(50)] public string ProjectName { get; set; }

        [Column("datecreated")] public DateTime? DateCreated { get; set; }

        [Column("expirationdate")] public DateTime? ExpirationDate { get; set; }

        [Column("paymentterms")] [MaxLength(100)] public string PaymentTerms { get; set; }

        [Column("status")] [MaxLength(50)] public string Status { get; set; }

        [Column("subtotal")] public decimal SubTotal { get; set; }

        [Column("discountamount")] public decimal DiscountAmount { get; set; }

        [Column("discountpercentage")] public decimal DiscountPercentage { get; set; }

        [Column("totaldiscount")] public decimal TotalDiscount { get; set; }

        [Column("taxpercentage")] public decimal TaxPercentage { get; set; }

        [Column("taxamount")] public decimal TaxAmount { get; set; }

        [Column("total")] public decimal Total { get; set; }

        [Column("reasonforclosing")] [MaxLength(100)] public string ReasonForClosing { get; set; }

        [Column("detailedreasonforclosing")] [MaxLength(100)] public string DetailedReasonForClosing { get; set; }

        [ForeignKey("Customer")]
        [Column("customer_id")] public int CustomerId { get; set; }

        [ForeignKey("Currency")]
        [Column("currencyid")] public int CurrencyId { get; set; }

        [Column("salespersonid")] public string SalesPersonId { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual ICollection<QuotationDetail> QuotationDetails { get; set; }

        public virtual ICollection<Note> QuotationNotes { get; set; }

        public virtual ICollection<File> QuotationFiles { get; set; }
    }
}