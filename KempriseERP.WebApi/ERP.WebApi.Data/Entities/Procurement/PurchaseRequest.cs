﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities.Procurement
{
    [Table("PurchaseRequest")]
    public class PurchaseRequest : AuditedEntity
    {
        [Column("purchaserequest_id")] [Key] public int PurchaseRequestId { get; set; }

        [ForeignKey("Supplier")]
        [Column("supplier_id")] public int SupplierId { get; set; }

        [ForeignKey("Currency")]
        [Column("currencyid")] public int CurrencyId { get; set; }

        [Column("purchaserepresentative_id")] public string PurchaseRepresentativeId { get; set; }

        [Column("purchaserequestnumber")] [MaxLength(50)] public string PurchaseRequestNumber { get; set; }

        [Column("projectname")] [MaxLength(50)] public string ProjectName { get; set; }

        [Column("datecreated")] public DateTime? DateCreated { get; set; }

        [Column("status")] [MaxLength(50)] public string Status { get; set; }

        [Column("reasonforclosing")] [MaxLength(100)] public string ReasonForClosing { get; set; }

        public virtual ICollection<PurchaseRequestDetail> PurchaseRequestDetails { get; set; }

        public virtual ICollection<File> PurchaseRequestFiles { get; set; }

        public virtual Supplier Supplier { get; set; }

        public virtual Currency Currency { get; set; }
    }
}