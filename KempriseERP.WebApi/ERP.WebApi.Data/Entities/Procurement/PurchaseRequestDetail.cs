﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities.Procurement
{
    [Table("PurchaseRequestDetail")]
    public class PurchaseRequestDetail
    {
        [Column("purchaserequestdetail_id")] [Key] public int PurchaseRequestDetailId { get; set; }

        [ForeignKey("PurchaseRequest")]
        [Column("purchaserequest_id")] public int PurchaseRequestId { get; set; }

        [Column("description")] [MaxLength(200)] public string Description { get; set; }
        
        [ForeignKey("Product")]
        [Column("product_id")] public int ProductId { get; set; }

        [Column("category")] [MaxLength(50)] public string Category { get; set; }

        [Column("quantity")] public decimal Quantity { get; set; }

        [Column("unitprice")] public decimal UnitPrice { get; set; }

        public virtual Product Product { get; set; }

        public virtual PurchaseRequest PurchaseRequest { get; set; }
    }
}