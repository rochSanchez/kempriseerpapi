﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities.Procurement
{
    [Table("PurchaseOrder")]
    public class PurchaseOrder
    {

    }

    [Table("PurchaseOrderDetails")]
    public class PurchaseOrderDetails
    {

    }

    [Table("PurchaseRfq")]
    public class PurchaseRfq
    {

    }
    [Table("PurchaseRfqDetails")]
    public class PurchaseRfqDetails
    {

    }

    [Table("PartAttribute")]
    public class PartAttribute
    {
        [Column("partattribute_id")] [Key] public int PartAttributeId { get; set; }
    }

    public class ProductPartAttribute
    {
        [Column("productpartattribute_id")] [Key] public int ProductPartAttributeId { get; set; }
        [Column("partattribute_id")] public int PartAttributeId { get; set; }
        [Column("product_id")] public int ProductId { get; set; }
    }

    //service, returns, office consumables, manufacturing consumables, raw materials, outsourced machine parts, buy parts, outsource assembly parts,
    [Table("ProductType")]
    public class ProductType
    {
        [Column("productyype_id")] [Key] public int ProductTypeId { get; set; }

        [Column("producttypename")] public string ProductyTypeName { get; set; }
    }

    [Table("Product")]
    public class Product
    {
        [Column("product_id")] [Key] public int ProductId { get; set; }

        [ForeignKey("ProductType")]
        [Column("productyype_id")] public int ProductTypeId { get; set; }

        [ForeignKey("Tax")]
        [Column("tax_id")] public int TaxId { get; set; }

        [ForeignKey("Supplier")]
        [Column("supplier_id")] public int SupplierId { get; set; }

        [Column("productname")] public string ProductName { get; set; }

        [Column("partnumber")] [MaxLength(50)] public string PartNumber { get; set; }

        [Column("description")] [MaxLength(200)] public string Description { get; set; }

        [Column("partcategory")] [MaxLength(50)] public string PartCategory { get; set; }

        [Column("price")] public decimal Price { get; set; }

        [Column("canbesold")] public bool CanBeSold { get; set; }

        [Column("canbepurchased")] public bool CanBePurchased { get; set; }

        [Column("requirespartnumber")] public bool RequiresPartNumber { get; set; }
    }
}