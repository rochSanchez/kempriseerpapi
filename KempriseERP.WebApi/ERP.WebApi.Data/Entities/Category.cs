﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Category")]
    public class Category : AuditedEntity
    {
        [Column("category_id")] [Key] public int CategoryId { get; set; }

        [Column("categoryname")] [MaxLength(50)] public string CategoryName { get; set; }
    }
}