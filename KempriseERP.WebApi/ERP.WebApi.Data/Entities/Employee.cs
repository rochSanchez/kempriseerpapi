﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Employee")]
    public class Employee : AuditedEntity
    {
        [Column("employee_id")] [Key] public int EmployeeId { get; set; }

        [Column("employeenumber")] [MaxLength(50)] public string EmployeeNumber { get; set; }

        [Column("name")] [MaxLength(50)] public string Name { get; set; }

        [Column("comments")] [MaxLength(300)] public string Comments { get; set; }

        public virtual ICollection<File> EmployeeFiles { get; set; }
    }
}