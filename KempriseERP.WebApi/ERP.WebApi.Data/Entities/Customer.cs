﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Customer")]
    public class Customer : AuditedEntity
    {
        [Column("customer_id")] [Key] public int CustomerId { get; set; }

        [Column("customernumber")] [MaxLength(50)] public string CustomerNumber { get; set; }

        [ForeignKey("Company")]
        [Column("company_id")] public int? CompanyId { get; set; }

        [Column("name")] [MaxLength(50)] public string Name { get; set; }

        [Column("designation")] [MaxLength(50)] public string Designation { get; set; }

        [Column("mobilenumber")] [MaxLength(50)] public string MobileNumber { get; set; }

        [Column("taxid")] [MaxLength(50)] public string TaxId { get; set; }

        [Column("email")] [MaxLength(50)] public string Email { get; set; }

        [Column("comments")] [MaxLength(300)] public string Comments { get; set; }

        public virtual Company Company { get; set; }

        public virtual ICollection<Inquiry> Inquiries { get; set; }
    }
}