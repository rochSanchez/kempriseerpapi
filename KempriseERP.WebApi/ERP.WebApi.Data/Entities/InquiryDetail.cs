﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("InquiryDetail")]
    public class InquiryDetail : AuditedEntity
    {
        [Column("inquirydetail_id")] [Key] public int InquiryDetailId { get; set; }

        [Column("description")] [MaxLength(200)] public string Description { get; set; }

        [Column("category")] [MaxLength(50)] public string Category { get; set; }

        [Column("quantity")] public decimal Quantity { get; set; }

        [ForeignKey("Inquiry")]
        [Column("inquiry_id")] public int InquiryId { get; set; }

        [ForeignKey("Part")]
        [Column("part_id")] public int? PartId { get; set; }

        public virtual Inquiry Inquiry { get; set; }

        public virtual Part Part { get; set; }
    }
}