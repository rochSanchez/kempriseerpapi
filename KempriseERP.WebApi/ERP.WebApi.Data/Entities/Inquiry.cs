﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Inquiry")]
    public class Inquiry : AuditedEntity
    {
        [Column("inquiry_id")] [Key] public int InquiryId { get; set; }

        [Column("inquirynumber")] [MaxLength(50)] public string InquiryNumber { get; set; }

        [Column("projectname")] [MaxLength(50)] public string ProjectName { get; set; }

        [Column("datecreated")] public DateTime? DateCreated { get; set; }

        [Column("expirationdate")] public DateTime? ExpirationDate { get; set; }

        [Column("paymentterms")] [MaxLength(100)] public string PaymentTerms { get; set; }

        [Column("status")] [MaxLength(50)] public string Status { get; set; }

        [Column("reasonforclosing")] [MaxLength(100)] public string ReasonForClosing { get; set; }

        [Column("detailedreasonforclosing")] [MaxLength(100)] public string DetailedReasonForClosing { get; set; }

        [ForeignKey("Customer")]
        [Column("customer_id")] public int CustomerId { get; set; }

        [ForeignKey("Currency")]
        [Column("currencyid")] public int CurrencyId { get; set; }

        [Column("salespersonid")] public string SalesPersonId { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual Currency Currency { get; set; }

        public virtual ICollection<InquiryDetail> InquiryDetails { get; set; }

        public virtual ICollection<File> InquiryFiles { get; set; }
    }
}