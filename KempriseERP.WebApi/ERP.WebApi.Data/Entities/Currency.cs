﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Currency")]
    public class Currency : AuditedEntity
    {
        [Column("currency_id")] [Key] public int CurrencyId { get; set; }

        [Column("name")] [MaxLength(50)] public string Name { get; set; }

        [Column("code")] [MaxLength(20)] public string Code { get; set; }

        [Column("comments")] [MaxLength(300)] public string Comments { get; set; }
    }
}