﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Supplier")]
    public class Supplier : AuditedEntity
    {
        [Column("supplier_id")] [Key] public int SupplierId { get; set; }

        [Column("suppliernumber")] [MaxLength(50)] public string SupplierNumber { get; set; }

        [Column("name")] [MaxLength(50)] public string Name { get; set; }

        [Column("supplieraddress")] [MaxLength(100)] public string SupplierAddress { get; set; }

        [Column("comments")] [MaxLength(300)] public string Comments { get; set; }

        [ForeignKey("Company")]
        [Column("company_id")] public int? CompanyId { get; set; }

        public virtual Company Company { get; set; }
    }
}