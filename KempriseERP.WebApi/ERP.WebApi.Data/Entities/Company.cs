﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    public class Company : AuditedEntity
    {
        [Column("company_id")] [Key] public int CompanyId { get; set; }

        [Column("name")] [MaxLength(50)] public string Name { get; set; }

        [Column("companyaddress")] [MaxLength(100)] public string CompanyAddress { get; set; }

        [Column("officenumber")] [MaxLength(50)] public string OfficeNumber { get; set; }

        [Column("email")] [MaxLength(50)] public string Email { get; set; }

        [Column("website")] [MaxLength(50)] public string Website { get; set; }

        [Column("comments")] [MaxLength(300)] public string Comments { get; set; }

        public virtual ICollection<Customer> Customers { get; set; }

        public virtual ICollection<Supplier> Suppliers { get; set; }
    }
}