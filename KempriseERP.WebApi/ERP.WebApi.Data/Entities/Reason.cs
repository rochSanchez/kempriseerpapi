﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Reason")]
    public class Reason : AuditedEntity
    {
        [Column("reason_id")] [Key] public int ReasonId { get; set; }

        [Column("name")] [MaxLength(50)] public string Name { get; set; }
    }
}