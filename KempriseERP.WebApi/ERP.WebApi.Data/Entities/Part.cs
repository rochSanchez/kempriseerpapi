﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Part")]
    public class Part : AuditedEntity
    {
        [Column("part_id")] [Key] public int PartId { get; set; }

        [Column("partnumber")] [MaxLength(50)] public string PartNumber { get; set; }

        [Column("description")] [MaxLength(200)] public string Description { get; set; }

        [Column("category")] [MaxLength(50)] public string Category { get; set; }

        [Column("price")] public decimal Price { get; set; }
    }
}