﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.WebApi.Data.Entities
{
    [Table("Note")]
    public class Note : AuditedEntity
    {
        [Column("note_id")] [Key] public int NoteId { get; set; }

        [Column("item")] public decimal Item { get; set; }

        [Column("name")] [MaxLength(500)] public string Name { get; set; }

        public virtual Quotation Quotation { get; set; }
    }
}