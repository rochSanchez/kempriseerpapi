﻿using System;

namespace ERP.WebApi.Data.Entities
{
    public class AuditedEntity
    {
        public int? Sequence { get; set; }

        public DateTime? UpdatedAt { get; set; }

        public string UpdatedById { get; set; }

        public string UpdatedByFirstName { get; set; }

        public string UpdatedByLastName { get; set; }

        public string UpdatedByFullName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string CreatedById { get; set; }

        public string CreatedByFirstName { get; set; }

        public string CreatedByLastName { get; set; }

        public string CreatedByFullName { get; set; }
    }
}