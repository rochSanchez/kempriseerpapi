﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface IInquiryDetailRepository : IErpRepository<InquiryDetail>
    {
    }

    public class InquiryDetailRepository : ErpRepositoryBase<InquiryDetail, ErpDbContext>, IInquiryDetailRepository
    {
        public InquiryDetailRepository(ErpDbContext context, ILogger<InquiryDetailRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}