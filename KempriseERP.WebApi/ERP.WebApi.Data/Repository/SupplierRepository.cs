﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface ISupplierRepository : IErpRepository<Supplier>
    {
    }

    public class SupplierRepository : ErpRepositoryBase<Supplier, ErpDbContext>, ISupplierRepository
    {
        public SupplierRepository(ErpDbContext context, ILogger<SupplierRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}