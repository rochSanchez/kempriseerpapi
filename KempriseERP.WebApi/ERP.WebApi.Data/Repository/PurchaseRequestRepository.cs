﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.WebApi.Data.Entities.Procurement;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface IPurchaseRequestRepository : IErpRepository<PurchaseRequest>
    {
        Task<List<PurchaseRequest>> PurchaseRequestGetRecList();

        Task<PurchaseRequest> PurchaseRequestGetRec(int purchaseRequestId);
    }

    public class PurchaseRequestRepository : ErpRepositoryBase<PurchaseRequest, ErpDbContext>, IPurchaseRequestRepository
    {
        public PurchaseRequestRepository(ErpDbContext context, ILogger<PurchaseRequestRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
        
        public async Task<List<PurchaseRequest>> PurchaseRequestGetRecList()
        {
            return await Context.PurchaseRequests
                .Include(x => x.Supplier)
                .ThenInclude(x => x.Company)
                .Include(x => x.Currency)
                .OrderByDescending(x => x.DateCreated)
                .AsSplitQuery()
                .ToListAsync();
        }

        public async Task<PurchaseRequest> PurchaseRequestGetRec(int purchaseRequestId)
        {
            return await Context.PurchaseRequests.Where(x => x.PurchaseRequestId == purchaseRequestId)
                .Include(x => x.Supplier)
                .ThenInclude(x => x.Company)
                .Include(x => x.Currency)
                .Include(x => x.PurchaseRequestDetails)
                .ThenInclude(x => x.Product)
                .Include(x => x.PurchaseRequestFiles)
                .AsSplitQuery()
                .FirstOrDefaultAsync();
        }
    }
}