﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface ICategoryRepository : IErpRepository<Category>
    {
    }

    public class CategoryRepository : ErpRepositoryBase<Category, ErpDbContext>, ICategoryRepository
    {
        public CategoryRepository(ErpDbContext context, ILogger<CategoryRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}