﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface ICurrencyRepository : IErpRepository<Currency>
    {
    }

    public class CurrencyRepository : ErpRepositoryBase<Currency, ErpDbContext>, ICurrencyRepository
    {
        public CurrencyRepository(ErpDbContext context, ILogger<CurrencyRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}