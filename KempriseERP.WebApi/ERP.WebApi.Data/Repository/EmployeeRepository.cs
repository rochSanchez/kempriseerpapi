﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository
{
    public interface IEmployeeRepository : IErpRepository<Employee>
    {
        Task<Employee> EmployeeGetRec(int employeeId);
    }

    public class EmployeeRepository : ErpRepositoryBase<Employee, ErpDbContext>, IEmployeeRepository
    {
        public EmployeeRepository(ErpDbContext context, ILogger<EmployeeRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }

        public async Task<Employee> EmployeeGetRec(int employeeId)
        {
            return await Context.Employees.Where(x => x.EmployeeId == employeeId)
                .Include(x => x.EmployeeFiles)
                .AsSplitQuery()
                .FirstOrDefaultAsync();
        }
    }
}