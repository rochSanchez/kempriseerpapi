﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface IReasonRepository : IErpRepository<Reason>
    {
    }

    public class ReasonRepository : ErpRepositoryBase<Reason, ErpDbContext>, IReasonRepository
    {
        public ReasonRepository(ErpDbContext context, ILogger<ReasonRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}