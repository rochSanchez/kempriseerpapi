﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface IFileRepository : IErpRepository<File>
    {
    }

    public class FileRepository : ErpRepositoryBase<File, ErpDbContext>, IFileRepository
    {
        public FileRepository(ErpDbContext context, ILogger<FileRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}