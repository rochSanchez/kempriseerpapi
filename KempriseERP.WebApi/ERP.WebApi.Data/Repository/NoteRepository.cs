﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface INoteRepository : IErpRepository<Note>
    {
    }

    public class NoteRepository : ErpRepositoryBase<Note, ErpDbContext>, INoteRepository
    {
        public NoteRepository(ErpDbContext context, ILogger<NoteRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}