﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository
{
    public interface IQuotationRepository : IErpRepository<Quotation>
    {
        Task<List<Quotation>> QuotationGetRecList();

        Task<Quotation> QuotationGetRec(int quotationId);

        Task<int> GetVersionCount(string quotationNumber);
    }

    public class QuotationRepository : ErpRepositoryBase<Quotation, ErpDbContext>, IQuotationRepository
    {
        public QuotationRepository(ErpDbContext context, ILogger<QuotationRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }

        public async Task<List<Quotation>> QuotationGetRecList()
        {
            return await Context.Quotations
                .Include(x => x.Customer)
                    .ThenInclude(x => x.Company)
                .Include(x => x.Currency)
                .OrderByDescending(x => x.DateCreated)
                .AsSplitQuery()
                .ToListAsync();
        }

        public async Task<Quotation> QuotationGetRec(int quotationId)
        {
            return await Context.Quotations.Where(x => x.QuotationId == quotationId)
                .Include(x => x.Customer)
                    .ThenInclude(x => x.Company)
                .Include(x => x.Currency)
                .Include(x => x.QuotationDetails)
                    .ThenInclude(x => x.Part)
                .Include(x => x.QuotationFiles)
                .Include(x => x.QuotationNotes)
                .AsSplitQuery()
                .FirstOrDefaultAsync();
        }

        public async Task<int> GetVersionCount(string quotationNumber)
        {
            return await Context.Quotations.Where(x => x.QuotationNumber == quotationNumber)
                .CountAsync();
        }
    }
}