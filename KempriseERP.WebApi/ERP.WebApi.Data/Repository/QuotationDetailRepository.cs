﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface IQuotationDetailRepository : IErpRepository<QuotationDetail>
    {
    }

    public class QuotationDetailRepository : ErpRepositoryBase<QuotationDetail, ErpDbContext>, IQuotationDetailRepository
    {
        public QuotationDetailRepository(ErpDbContext context, ILogger<QuotationDetailRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}