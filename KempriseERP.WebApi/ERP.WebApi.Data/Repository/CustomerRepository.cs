﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository
{
    public interface ICustomerRepository : IErpRepository<Customer>
    {
        Task<List<Customer>> CustomerGetRecList();

        Task<Customer> CustomerGetRec(int customerId);
    }

    public class CustomerRepository : ErpRepositoryBase<Customer, ErpDbContext>, ICustomerRepository
    {
        public CustomerRepository(ErpDbContext context, ILogger<CustomerRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }

        public async Task<List<Customer>> CustomerGetRecList()
        {
            return await Context.Customers
                .Include(x => x.Company)
                .OrderByDescending(x => x.UpdatedAt)
                .AsSplitQuery()
                .ToListAsync();
        }

        public async Task<Customer> CustomerGetRec(int customerId)
        {
            return await Context.Customers.Where(x => x.CustomerId == customerId)
                .Include(x => x.Inquiries)
                .Include(x => x.Company)
                .AsSplitQuery()
                .FirstOrDefaultAsync();
        }
    }
}