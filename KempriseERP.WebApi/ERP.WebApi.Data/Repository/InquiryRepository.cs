﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository
{
    public interface IInquiryRepository : IErpRepository<Inquiry>
    {
        Task<List<Inquiry>> InquiryGetRecList();

        Task<Inquiry> InquiryGetRec(int inquiryId);
    }

    public class InquiryRepository : ErpRepositoryBase<Inquiry, ErpDbContext>, IInquiryRepository
    {
        public InquiryRepository(ErpDbContext context, ILogger<InquiryRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }

        public async Task<List<Inquiry>> InquiryGetRecList()
        {
            return await Context.Inquiries
                .Include(x => x.Customer)
                    .ThenInclude(x => x.Company)
                .Include(x => x.Currency)
                .OrderByDescending(x => x.DateCreated)
                .AsSplitQuery()
                .ToListAsync();
        }

        public async Task<Inquiry> InquiryGetRec(int inquiryId)
        {
            return await Context.Inquiries.Where(x => x.InquiryId == inquiryId)
                .Include(x => x.Customer)
                    .ThenInclude(x => x.Company)
                .Include(x => x.Currency)
                .Include(x => x.InquiryDetails)
                    .ThenInclude(x => x.Part)
                .Include(x => x.InquiryFiles)
                .AsSplitQuery()
                .FirstOrDefaultAsync();
        }
    }
}