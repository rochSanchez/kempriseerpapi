﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface IPartRepository : IErpRepository<Part>
    {
    }

    public class PartRepository : ErpRepositoryBase<Part, ErpDbContext>, IPartRepository
    {
        public PartRepository(ErpDbContext context, ILogger<PartRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}