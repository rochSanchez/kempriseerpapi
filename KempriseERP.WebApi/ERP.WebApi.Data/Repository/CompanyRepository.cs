﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Persistence;
using ERP.WebApi.Data.Repository.Base;
using Microsoft.Extensions.Logging;

namespace ERP.WebApi.Data.Repository
{
    public interface ICompanyRepository : IErpRepository<Company>
    {
    }

    public class CompanyRepository : ErpRepositoryBase<Company, ErpDbContext>, ICompanyRepository
    {
        public CompanyRepository(ErpDbContext context, ILogger<CompanyRepository> logger, IUserContext userContext) :
            base(context, logger, userContext)
        {
        }
    }
}