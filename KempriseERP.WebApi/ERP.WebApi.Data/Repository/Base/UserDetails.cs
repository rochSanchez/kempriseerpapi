﻿namespace ERP.WebApi.Data.Repository.Base
{
    public class UserDetails
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string GmtUserId { get; set; }
    }
}