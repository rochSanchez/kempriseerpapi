﻿namespace ERP.WebApi.Data.Repository.Base
{
    public interface IUserContext
    {
        void SetDetails();

        UserDetails UserDetails { get; set; }
    }
}