﻿using ERP.WebApi.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository.Base
{
    public class ErpRepositoryBase<TEntity, TContext> : IErpRepository<TEntity>
        where TContext : DbContext
        where TEntity : AuditedEntity
    {
        private readonly ILogger _logger;
        private readonly IUserContext _userContext;
        internal DbSet<TEntity> dbSet;

        protected ErpRepositoryBase(TContext context, ILogger logger, IUserContext userContext)
        {
            _logger = logger;
            _userContext = userContext;
            Context = context;
            dbSet = context.Set<TEntity>();
        }

        protected TContext Context { get; set; }

        private bool HasChanges => Context.ChangeTracker.HasChanges();

        public virtual async Task<TEntity> GetRec(int id)
        {
            var entity = await Context.Set<TEntity>().FindAsync(id);
            return entity;
        }

        public async Task<IEnumerable<TEntity>> GetRecList()
        {
            return await Context.Set<TEntity>()
                .AsNoTracking()
                .OrderByDescending(x => x.UpdatedAt)
                .ToListAsync();
        }

        public async Task<bool> AddRec(TEntity model)
        {
            var transaction = await Context.Database.BeginTransactionAsync();
            try
            {
                var firstName = _userContext.UserDetails.FirstName.ToUpper();
                var lastName = _userContext.UserDetails.LastName.ToUpper();

                model.CreatedAt = DateTime.Now;
                model.CreatedByFirstName = firstName;
                model.CreatedByLastName = lastName;
                model.CreatedByFullName = $"{firstName} {lastName}";
                model.CreatedById = _userContext.UserDetails.GmtUserId;

                await transaction.CreateSavepointAsync("BeforeAddrec");
                await Context.Set<TEntity>().AddAsync(model);

                var result = await SaveChanges();
                await transaction.CommitAsync();

                return result;
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Debug, e.Message);

                await transaction.RollbackToSavepointAsync("BeforeAddrec");

                if (e.InnerException != null && e.InnerException.Message.Contains("Duplicate"))
                    throw new ApplicationException(e.InnerException.Message);

                throw new ApplicationException(e.Message);
            }
        }

        public async Task<bool> AddRecords(IEnumerable<TEntity> models)
        {
            var auditedEntities = models as TEntity[] ?? models.ToArray();
            foreach (var model in auditedEntities)
            {
                var firstName = _userContext.UserDetails.FirstName.ToUpper();
                var lastName = _userContext.UserDetails.LastName.ToUpper();

                model.CreatedAt = DateTime.Now;
                model.CreatedByFirstName = firstName;
                model.CreatedByLastName = lastName;
                model.CreatedByFullName = $"{firstName} {lastName}";
                model.CreatedById = _userContext.UserDetails.GmtUserId;
            }

            await Context.Set<TEntity>().AddRangeAsync(auditedEntities);

            return await SaveChanges();
        }

        public async Task<bool> DelRec(TEntity model)
        {
            Context.Entry(model).State = EntityState.Detached;
            Context.Set<TEntity>().Remove(model);
            return await SaveChanges();
        }

        public async Task<bool> ModiRec(TEntity model)
        {
            var firstName = _userContext.UserDetails.FirstName.ToUpper();
            var lastName = _userContext.UserDetails.LastName.ToUpper();

            model.UpdatedAt = DateTime.Now;
            model.UpdatedByFirstName = firstName;
            model.UpdatedByLastName = lastName;
            model.UpdatedByFullName = $"{firstName} {lastName}";
            model.UpdatedById = _userContext.UserDetails.GmtUserId;

            Context.Set<TEntity>().Update(model);
            Context.Entry(model).State = EntityState.Modified;

            return await SaveChanges();
        }

        public async Task<bool> ModiRecRange(IEnumerable<TEntity> models)
        {
            var auditedEntities = models as TEntity[] ?? models.ToArray();
            foreach (var model in auditedEntities)
            {
                var firstName = _userContext.UserDetails.FirstName.ToUpper();
                var lastName = _userContext.UserDetails.LastName.ToUpper();

                model.UpdatedAt = DateTime.Now;
                model.UpdatedByFirstName = firstName;
                model.UpdatedByLastName = lastName;
                model.UpdatedByFullName = $"{firstName} {lastName}";
                model.UpdatedById = _userContext.UserDetails.GmtUserId;
            }

            Context.Set<TEntity>().UpdateRange(auditedEntities);

            return await SaveChanges();
        }

        private async Task<bool> SaveChanges()
        {
            return await Context.SaveChangesAsync() >= 0;
        }

        public async Task<int> Count()
        {
            return await Context.Set<TEntity>().CountAsync();
        }

        public async Task<int> GetSequence()
        {
            var maxSequence = await Context.Set<TEntity>().MaxAsync(x => x.Sequence);
            var maxSeq = maxSequence == 0 || maxSequence == null ? 1 : maxSequence.Value + 1;

            var count = await Context.Set<TEntity>().CountAsync();

            return count >= maxSeq ? count : maxSeq;
        }

        public async Task<TEntity> GetLatestRecord()
        {
            return await Context.Set<TEntity>().OrderByDescending(x => x.Sequence).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }

        public virtual async Task<TEntity> GetSingleOrDefaultAsync(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            bool disableTracking = true) => await this.GetSingleOrDefaultAsync(entity => entity, includeChildren,
            filterBy, orderBy, disableTracking);

        public virtual async Task<TResult> GetSingleOrDefaultAsync<TResult>(
            Expression<Func<TEntity, TResult>> resultsSelector = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            bool disableTracking = true) => await this
            .GetAll(resultsSelector, includeChildren, filterBy, orderBy, disableTracking: disableTracking)
            .SingleOrDefaultAsync();

        public virtual async Task<TEntity> GetFirstOrDefaultAsync(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            bool disableTracking = true) => await this.GetFirstOrDefaultAsync(entity => entity, includeChildren,
            filterBy, orderBy, disableTracking);

        public virtual async Task<TResult> GetFirstOrDefaultAsync<TResult>(
            Expression<Func<TEntity, TResult>> resultsSelector = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            bool disableTracking = true) => await this
            .GetAll(resultsSelector, includeChildren, filterBy, orderBy, disableTracking: disableTracking)
            .FirstOrDefaultAsync();

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            int? pageNumber = default,
            int? pageSize = default,
            bool disableTracking = true,
            bool distinct = false) => await this.GetAllAsync(entity => entity, includeChildren, filterBy, orderBy,
            pageNumber, pageSize, disableTracking, distinct);

        public virtual async Task<IEnumerable<TResult>> GetAllAsync<TResult>(
            Expression<Func<TEntity, TResult>> resultsSelector,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            int? pageNumber = default,
            int? pageSize = default,
            bool disableTracking = true,
            bool distinct = false) => await this.GetAll(resultsSelector, includeChildren, filterBy, orderBy, pageNumber,
            pageSize, disableTracking, distinct).ToListAsync();

        public IQueryable<TResult> GetAll<TResult>(
            Expression<Func<TEntity, TResult>> resultsSelector,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            int? pageNumber = default,
            int? pageSize = default,
            bool disableTracking = true,
            bool distinct = false)
        {
            var query = this.dbSet as IQueryable<TEntity>;
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }

            return query.AdvancedSearch(resultsSelector, includeChildren, filterBy, orderBy, pageNumber, pageSize,
                distinct);
        }

        public IQueryable<TEntity> GetAll(
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeChildren = null,
            Expression<Func<TEntity, bool>> filterBy = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            int? pageNumber = default,
            int? pageSize = default,
            bool disableTracking = true,
            bool distinct = false)
        {
            var query = this.dbSet as IQueryable<TEntity>;
            if (disableTracking)
            {
                query = query.AsNoTracking();
            }

            return query.AdvancedSearch(includeChildren, filterBy, orderBy, pageNumber, pageSize, distinct);
        }

        public virtual TEntity Add(TEntity entity) => this.Context.Add(entity).Entity;

        public virtual void AddRange(IEnumerable<TEntity> entities) => this.Context.AddRange(entities);

        public virtual TEntity Update(TEntity entity) => this.Context.Update(entity).Entity;

        public virtual TEntity DeletePermanently(TEntity entity) => this.Context.Remove(entity).Entity;

        public virtual void DeleteRangePermanently(IEnumerable<TEntity> entities) => this.Context.RemoveRange(entities);

        public virtual void DeleteRangePermanently(Expression<Func<TEntity, bool>> filterBy) =>
            this.Context.RemoveRange(this.dbSet.Where(filterBy));

        public virtual async Task<TEntity> AddAndSaveAsync(TEntity entity,
            CancellationToken cancellationToken = default)
        {
            var result = this.Add(entity);
            await this.SaveChangesAsync(cancellationToken);
            return result;
        }

        public virtual async Task AddRangeAndSaveAsync(IEnumerable<TEntity> entities,
            CancellationToken cancellationToken = default)
        {
            this.AddRange(entities);
            await this.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task<TEntity> UpdateAndSaveAsync(TEntity entity,
            CancellationToken cancellationToken = default)
        {
            var result = this.Update(entity);
            await this.SaveChangesAsync(cancellationToken);
            return result;
        }

        public virtual async Task UpdateRangeAndSaveAsync(IEnumerable<TEntity> entities,
            CancellationToken cancellationToken = default)
        {
            foreach (var entity in entities)
            {
                this.Update(entity);
            }

            await this.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task<bool> DeletePermanentlyAndSaveAsync(TEntity entity,
            CancellationToken cancellationToken = default)
        {
            this.DeletePermanently(entity);
            var affectedRows = await this.SaveChangesAsync(cancellationToken);
            return affectedRows > 0;
        }

        public virtual async Task DeleteRangePermanentlyAndSaveAsync(Expression<Func<TEntity, bool>> filterBy,
            CancellationToken cancellationToken = default)
        {
            this.DeleteRangePermanently(filterBy);
            await this.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteRangePermanentlyAndSaveAsync(IEnumerable<TEntity> entities,
            CancellationToken cancellationToken = default)
        {
            this.DeleteRangePermanently(entities);
            await this.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) =>
            await this.Context.SaveChangesAsync(cancellationToken);

        public async Task<long> Count(Expression<Func<TEntity, bool>> filterBy = null)
            => filterBy == null
                ? await this.dbSet.CountAsync()
                : await this.dbSet.CountAsync(filterBy);

        public void Reload(TEntity entity)
        {
            this.Context.Entry(entity).Reload();
        }
    }
}