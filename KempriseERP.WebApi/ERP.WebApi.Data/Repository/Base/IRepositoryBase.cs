﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository.Base
{
    public interface IRepositoryBase<T>
    {
        T Add(T entity);

        void AddRange(IEnumerable<T> entities);

        Task<T> AddAndSaveAsync(T entity, CancellationToken cancellationToken = default);

        Task AddRangeAndSaveAsync(IEnumerable<T> entities, CancellationToken cancellationToken = default);

        T Update(T entity);

        Task<T> UpdateAndSaveAsync(T entity, CancellationToken cancellationToken = default);

        T DeletePermanently(T entity);

        void DeleteRangePermanently(IEnumerable<T> entities);

        void DeleteRangePermanently(Expression<Func<T, bool>> filterBy);

        Task DeleteRangePermanentlyAndSaveAsync(Expression<Func<T, bool>> filterBy,
            CancellationToken cancellationToken = default);

        Task<bool> DeletePermanentlyAndSaveAsync(T entity, CancellationToken cancellationToken = default);

        Task DeleteRangePermanentlyAndSaveAsync(IEnumerable<T> entities, CancellationToken cancellationToken = default);

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        Task<long> Count(Expression<Func<T, bool>> filterBy = null);

        void Reload(T entity);
    }
}