﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ERP.WebApi.Data.Repository.Base
{
    public interface IErpRepository<T> : IRepositoryBase<T>
    {
        Task<bool> AddRec(T model);

        Task<bool> AddRecords(IEnumerable<T> models);

        Task<bool> ModiRec(T model);

        Task<bool> ModiRecRange(IEnumerable<T> models);

        Task<bool> DelRec(T model);

        Task<T> GetRec(int id);

        Task<IEnumerable<T>> GetRecList();

        Task<int> Count();

        Task<int> GetSequence();

        Task<T> GetLatestRecord();

        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);
    }
}