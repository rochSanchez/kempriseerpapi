﻿using Microsoft.AspNetCore.Http;
using System.Linq;

namespace ERP.WebApi.Data.Repository.Base
{
    internal class UserContext : IUserContext
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            SetDetails();
        }

        public UserDetails UserDetails { get; set; }

        public void SetDetails()
        {
            if (_httpContextAccessor.HttpContext == null) return;

            var userName = _httpContextAccessor.HttpContext.User.Claims.SingleOrDefault(a =>
                a.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;
            var lastName = userName?.Split(".").Last();
            var firstName = userName?.Split(".").First();

            var userId = _httpContextAccessor.HttpContext.User.Claims.SingleOrDefault(a => a.Type == "uid")?.Value;
            UserDetails = new UserDetails { FirstName = firstName, LastName = lastName, GmtUserId = userId };
        }
    }
}