﻿using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace ERP.WebApi.Data.Repository.Base
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> AdvancedSearch<T>(
            this IQueryable<T> sourceCollection,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> includeChildren = default,
            Expression<Func<T, bool>> filterBy = default,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = default,
            int? pageNumber = default,
            int? pageSize = default,
            bool distinct = false)
            => AdvancedSearchForGivenCollection(sourceCollection, entity => entity, includeChildren, filterBy, orderBy, pageNumber, pageSize, distinct);

        public static IQueryable<TResult> AdvancedSearch<T, TResult>(
            this IQueryable<T> sourceCollection,
            Expression<Func<T, TResult>> resultsSelector,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> includeChildren = default,
            Expression<Func<T, bool>> filterBy = default,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = default,
            int? pageNumber = default,
            int? pageSize = default,
            bool distinct = false)
            => AdvancedSearchForGivenCollection(sourceCollection, resultsSelector, includeChildren, filterBy, orderBy, pageNumber, pageSize, distinct);

        private static IQueryable<TResult> AdvancedSearchForGivenCollection<T, TResult>(
            IQueryable<T> sourceCollection,
            Expression<Func<T, TResult>> resultsSelector,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> includeChildren = default,
            Expression<Func<T, bool>> filterBy = default,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = default,
            int? pageNumber = default,
            int? pageSize = default,
            bool distinct = false)
        {
            var query = sourceCollection;

            if (includeChildren != null)
            {
                query = includeChildren(query);
            }

            if (filterBy != null)
            {
                query = query.Where(filterBy);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (pageNumber.HasValue && pageSize.HasValue)
            {
                query = query
                    .Skip(pageNumber.Value)
                    .Take(pageSize.Value);
            }

            if (distinct)
            {
                query = query.Distinct();
            }

            return query.Select(resultsSelector);
        }
    }
}