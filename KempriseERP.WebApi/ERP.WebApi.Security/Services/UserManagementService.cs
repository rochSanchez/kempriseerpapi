﻿using ERP.WebApi.Business.Exceptions;
using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Security.Contracts;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.WebApi.Security.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _rolesManager;

        public UserManagementService(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> rolesManager)
        {
            _userManager = userManager;
            _rolesManager = rolesManager;
        }

        public async Task<string> CreateRoleAsync(string roleName)
        {
            if (string.IsNullOrEmpty(roleName))
            {
                throw new BadRequestException("Role Name Cannot Be Null or Empty");
            }

            var roleExist = await _rolesManager.RoleExistsAsync(roleName);
            if (roleExist)
            {
                throw new BadRequestException($"{roleName} Role Already Exist");
            }

            var result = await _rolesManager.CreateAsync(new ApplicationRole { Name = roleName });
            if (result.Succeeded)
            {
                return ($"{roleName} Created Successfully");
            }

            throw new BadRequestException(result.Errors.ToList().FirstOrDefault()?.ToString());
        }

        public async Task<UserRolesDto> AddUserToRoleAsync(UserToRoleDto addUserToRoleDto)
        {
            if (string.IsNullOrEmpty(addUserToRoleDto.RoleName))
            {
                throw new BadRequestException($"Role name cannot be null or empty");
            }

            var user = await _userManager.FindByNameAsync(addUserToRoleDto.UserName);
            var role = await _rolesManager.FindByNameAsync(addUserToRoleDto.RoleName.Replace(" ", ""));
            if (user == null)
                throw new NotFoundException($"Not found", addUserToRoleDto.UserName);

            if (role == null)
                throw new Exception($"{addUserToRoleDto.RoleName} Not found");

            var userIsInRole = await _userManager.IsInRoleAsync(user, role.NormalizedName);
            if (userIsInRole)
                throw new BadRequestException($"User is Already in role");

            var result = await _userManager.AddToRoleAsync(user, role.NormalizedName);
            if (result.Succeeded)
                return await GetUserRolesAsync(user.NormalizedUserName);

            throw new BadRequestException(result.Errors.ToList().FirstOrDefault()?.ToString());
        }

        public async Task<string> RemoveUserFromRoleAsync(UserToRoleDto addUserToRoleDto)
        {
            if (string.IsNullOrEmpty(addUserToRoleDto.RoleName))
            {
                throw new BadRequestException($"Role name cannot be null or empty");
            }

            var user = await _userManager.FindByNameAsync(addUserToRoleDto.UserName);
            var role = await _rolesManager.FindByNameAsync(addUserToRoleDto.RoleName.Replace(" ", ""));
            if (user == null)
                throw new NotFoundException("User Not found", addUserToRoleDto.UserName);

            if (role == null)
                throw new NotFoundException("Role Not found", addUserToRoleDto.RoleName);

            var userIsInRole = await _userManager.IsInRoleAsync(user, role.NormalizedName);
            if (!userIsInRole)
                throw new BadRequestException($"User Not In Role");

            var result = await _userManager.RemoveFromRoleAsync(user, role.NormalizedName);
            if (result.Succeeded)
                return ($"User Removed from role");

            throw new BadRequestException(result.Errors.ToList().FirstOrDefault()?.ToString());
        }

        public async Task<UserRolesDto> GetUserRolesAsync(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                throw new NotFoundException("User not found", userName);

            var userRoles = await _userManager.GetRolesAsync(user);
            var userRolesList = new List<UserRole>();
            foreach (var userRole in userRoles)
            {
                var role = await _rolesManager.Roles.FirstOrDefaultAsync(x => x.Name == userRole);
                userRolesList.Add(new UserRole() { RoleCategory = role.RoleCategory, Name = role.Name });
            }

            var allRoles = _rolesManager.Roles;
            var availableRoles = new List<UserRole>();
            foreach (var roleName in allRoles)
            {
                if (userRolesList.Any(x => x.Name == roleName.Name)) continue;
                availableRoles.Add(new UserRole { RoleCategory = roleName.RoleCategory, Name = roleName.Name });
            }

            var userRoleObject = new UserRolesDto()
            {
                Roles = availableRoles,
                UserRoles = userRolesList,
                User = new UserDto
                {
                    FullName = $"{user.LastName} {user.FirstName}",
                    Email = user.Email,
                    UserName = user.UserName,
                    UserId = user.Id
                }
            };

            return userRoleObject;
        }

        public async Task<UserRolesDto> GetUserRolesByIdAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new NotFoundException("User not found", userId);

            var userRoles = await _userManager.GetRolesAsync(user);
            var userRolesList = new List<UserRole>();
            foreach (var userRole in userRoles)
            {
                var role = await _rolesManager.Roles.FirstOrDefaultAsync(x => x.Name == userRole);
                userRolesList.Add(new UserRole() { RoleCategory = role.RoleCategory, Name = role.Name });
            }

            var allRoles = _rolesManager.Roles;
            var availableRoles = new List<UserRole>();
            foreach (var roleName in allRoles)
            {
                if (userRolesList.Any(x => x.Name == roleName.Name)) continue;
                availableRoles.Add(new UserRole { RoleCategory = roleName.RoleCategory, Name = roleName.Name });
            }

            var firstName = char.ToUpper(user.FirstName[0]) + user.FirstName.Substring(1);
            var lastName = char.ToUpper(user.LastName[0]) + user.LastName.Substring(1);

            var userRoleObject = new UserRolesDto
            {
                Roles = availableRoles,
                UserRoles = userRolesList,
                User = new UserDto
                {
                    FullName = $"{lastName}, {firstName}",
                    Email = user.Email,
                    UserName = user.UserName,
                    UserId = user.Id
                }
            };

            return userRoleObject;
        }

        public async Task<List<UserDto>> GetUserByRoleCategory(RoleCategory roleCategory)
        {
            var roles = await _rolesManager.Roles.Where(x => x.RoleCategory == roleCategory).ToListAsync();
            var usersInRole = new List<UserDto>();
            foreach (var applicationRole in roles)
            {
                var users = await _userManager.GetUsersInRoleAsync(applicationRole.NormalizedName);
                foreach (var user in users)
                {
                    if (usersInRole.Any(x => x.UserName == user.UserName)) continue;
                    var firstName = char.ToUpper(user.FirstName[0]) + user.FirstName.Substring(1);
                    var lastName = char.ToUpper(user.LastName[0]) + user.LastName.Substring(1);
                    usersInRole.Add(new UserDto
                    {
                        FullName = $"{lastName}, {firstName}",
                        Email = user.Email,
                        UserName = user.UserName,
                        UserId = user.Id,
                        FirstName = firstName,
                        LastName = lastName,
                        PhoneNumber = user.PhoneNumber
                    });
                }
            }

            return usersInRole;
        }

        public async Task<List<UserDto>> GetUsers()
        {
            var users = new List<UserDto>();
            var applicationUsers = await _userManager.Users.ToListAsync();

            foreach (var applicationUser in applicationUsers)
            {
                var userRoles = await _userManager.GetRolesAsync(applicationUser);
                if (userRoles.Contains("SUPER USER")) continue;

                var firstName = char.ToUpper(applicationUser.FirstName[0]) + applicationUser.FirstName.Substring(1);
                var lastName = char.ToUpper(applicationUser.LastName[0]) + applicationUser.LastName.Substring(1);

                users.Add(new UserDto
                {
                    FullName = $"{lastName}, {firstName}",
                    Email = applicationUser.Email,
                    UserName = applicationUser.UserName,
                    UserId = applicationUser.Id,
                    FirstName = firstName,
                    LastName = lastName,
                    PhoneNumber = applicationUser.PhoneNumber,
                    UserPhoto = applicationUser.UserPhoto
                });
            }

            return users;
        }

        public async Task<List<UserDto>> GetUsersByManager(string username)
        {
            var users = new List<UserDto>();
            var managerUser = await _userManager.FindByNameAsync(username);
            var managerRoles = await _userManager.GetRolesAsync(managerUser);
            var roleCategories = await _rolesManager.Roles.Where(x => managerRoles.Contains(x.Name)).Select(x => x.RoleCategory)
                .Distinct().ToListAsync();
            var roles = await _rolesManager.Roles.Where(x => roleCategories.Contains(x.RoleCategory)).ToListAsync();

            foreach (var role in roles)
            {
                if (role.Name.Contains("MANAGER")) continue;
                var applicationUsers = await _userManager.GetUsersInRoleAsync(role.NormalizedName);
                foreach (var applicationUser in applicationUsers)
                {
                    if (users.Any(x => x.UserName == applicationUser.UserName)) continue;
                    var firstName = char.ToUpper(applicationUser.FirstName[0]) + applicationUser.FirstName.Substring(1);
                    var lastName = char.ToUpper(applicationUser.LastName[0]) + applicationUser.LastName.Substring(1);

                    users.Add(new UserDto
                    {
                        FullName = $"{lastName}, {firstName}",
                        Email = applicationUser.Email,
                        UserName = applicationUser.UserName,
                        UserId = applicationUser.Id,
                        FirstName = firstName,
                        LastName = lastName,
                        PhoneNumber = applicationUser.PhoneNumber
                    });
                }
            }

            return users;
        }

        public async Task<UserDto> GetUserByIdAsync(string userId)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == userId);
            var firstName = char.ToUpper(user.FirstName[0]) + user.FirstName.Substring(1);
            var lastName = char.ToUpper(user.LastName[0]) + user.LastName.Substring(1);

            return new UserDto
            {
                FullName = $"{lastName}, {firstName}",
                Email = user.Email,
                UserName = user.UserName,
                UserId = user.Id,
                FirstName = firstName,
                LastName = lastName,
                PhoneNumber = user.PhoneNumber
            };
        }

        public async Task<List<UserDto>> GetConcernedUsers(string[] roleNames)
        {
            var concernedUsers = new List<UserDto>();
            var allUsers = await _userManager.Users.ToListAsync();

            foreach (var user in allUsers)
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                concernedUsers.AddRange(from roleName in roleNames
                                        where userRoles.Contains(roleName)
                                        select char.ToUpper(user.FirstName[0]) + user.FirstName.Substring(1)
                                        into firstName
                                        let lastName = char.ToUpper(user.LastName[0]) + user.LastName.Substring(1)
                                        select new UserDto
                                        {
                                            FullName = $"{lastName}, {firstName}",
                                            Email = user.Email,
                                            UserName = user.UserName,
                                            UserId = user.Id,
                                            FirstName = firstName,
                                            LastName = lastName,
                                            PhoneNumber = user.PhoneNumber,
                                            UserPhoto = user.UserPhoto
                                        });
            }

            return concernedUsers;
        }
    }
}