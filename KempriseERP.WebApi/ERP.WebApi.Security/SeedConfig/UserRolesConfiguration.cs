﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.WebApi.Security.SeedConfig
{
    public class UserRolesConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        private const string SuperUserId = "78b68e1d-b75d-4457-ab11-5af442c45aaf";
        private const string SuperUserRoleId = "badc6a21-83c5-444b-9e9a-54ab93625af9";

        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {
            var iur = new IdentityUserRole<string>
            {
                RoleId = SuperUserRoleId,
                UserId = SuperUserId
            };

            builder.HasData(iur);
        }
    }
}