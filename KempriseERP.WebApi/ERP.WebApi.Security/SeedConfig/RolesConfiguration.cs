﻿using ERP.WebApi.DTO.DTOs.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace ERP.WebApi.Security.SeedConfig
{
    public class RolesConfiguration : IEntityTypeConfiguration<ApplicationRole>
    {
        public void Configure(EntityTypeBuilder<ApplicationRole> builder)
        {
            var superUserId = "badc6a21-83c5-444b-9e9a-54ab93625af9";
            builder.HasData(new ApplicationRole
            {
                Name = "SUPER USER",
                NormalizedName = "SUPERUSER",
                Id = superUserId
            });

            //Sales Roles
            builder.HasData(new ApplicationRole
            {
                Name = "SALES ADMIN",
                NormalizedName = "SALESADMIN",
                Id = Guid.NewGuid().ToString(),
                RoleCategory = RoleCategory.Sales
            });

            builder.HasData(new ApplicationRole
            {
                Name = "SALES ENGINEER",
                NormalizedName = "SALESENGINEER",
                Id = Guid.NewGuid().ToString(),
                RoleCategory = RoleCategory.Sales
            });

            builder.HasData(new ApplicationRole
            {
                Name = "SENIOR SALES",
                NormalizedName = "SENIORSALES",
                Id = Guid.NewGuid().ToString(),
                RoleCategory = RoleCategory.Sales
            });

            builder.HasData(new ApplicationRole
            {
                Name = "SALES MANAGER",
                NormalizedName = "SALESMANAGER",
                Id = Guid.NewGuid().ToString(),
                RoleCategory = RoleCategory.Sales
            });
        }
    }
}