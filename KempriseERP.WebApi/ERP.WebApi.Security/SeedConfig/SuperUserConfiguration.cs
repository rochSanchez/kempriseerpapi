﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ERP.WebApi.Security.SeedConfig
{
    public class SuperUserConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var dev = new ApplicationUser
            {
                Id = "78b68e1d-b75d-4457-ab11-5af442c45aaf",
                Email = "dev@gmail.com",
                EmailConfirmed = true,
                FirstName = "Erp",
                LastName = "Dev",
                UserName = "Erp.Dev",
                NormalizedUserName = "Erp.Dev",
            };

            dev.PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(dev, "dev");
            builder.HasData(dev);
        }
    }
}