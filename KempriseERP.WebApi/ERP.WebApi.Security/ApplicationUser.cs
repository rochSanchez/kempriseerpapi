﻿using Microsoft.AspNetCore.Identity;

namespace ERP.WebApi.Security
{
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public byte[] UserPhoto { get; set; }
    }
}