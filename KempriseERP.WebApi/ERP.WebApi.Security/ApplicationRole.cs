﻿using ERP.WebApi.DTO.DTOs.Authorization;
using Microsoft.AspNetCore.Identity;

namespace ERP.WebApi.Security
{
    public class ApplicationRole : IdentityRole
    {
        public RoleCategory RoleCategory { get; set; }
    }
}