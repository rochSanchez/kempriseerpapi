﻿using AutoMapper;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Customers.Commands
{
    public class CustomerModiRecCommand : IRequest<TransactionResult<CustomerDto>>
    {
        public CustomerDto CustomerDto { get; set; }
    }

    public class CustomerModiRecHandler : IRequestHandler<CustomerModiRecCommand, TransactionResult<CustomerDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepo;

        public CustomerModiRecHandler(IMapper mapper, ICustomerRepository customerRepo)
        {
            _customerRepo = customerRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<CustomerDto>> Handle(CustomerModiRecCommand request, CancellationToken cancellationToken)
        {
            var dataToSave = _mapper.Map<Customer>(request.CustomerDto);
            var modiRecResult = await _customerRepo.ModiRec(dataToSave);

            return new TransactionResult<CustomerDto>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}