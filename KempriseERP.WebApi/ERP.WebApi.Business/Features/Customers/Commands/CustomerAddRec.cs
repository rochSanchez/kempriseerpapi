﻿using AutoMapper;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Customers.Commands
{
    public class CustomerAddRecCommand : IRequest<TransactionResult<CustomerDto>>
    {
        public CustomerDto CustomerDto { get; set; }
    }

    public class CustomerAddRecHandler : IRequestHandler<CustomerAddRecCommand, TransactionResult<CustomerDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepo;

        public CustomerAddRecHandler(IMapper mapper, ICustomerRepository customerRepo)
        {
            _mapper = mapper;
            _customerRepo = customerRepo;
        }

        public async Task<TransactionResult<CustomerDto>> Handle(CustomerAddRecCommand request, CancellationToken cancellationToken)
        {
            var customerEntity = _mapper.Map<Customer>(request.CustomerDto);

            var latestRecord = await _customerRepo.GetLatestRecord();
            var addRecResult = await _customerRepo.AddRec(customerEntity);

            var sequence = await _customerRepo.GetSequence();
            customerEntity.Sequence = sequence;

            if (sequence == 1)
                customerEntity.CustomerNumber = "17-1001";
            else
            {
                var splitNumber = latestRecord.CustomerNumber.Split('-');
                var recordNumber = Convert.ToDecimal(splitNumber[1]);
                customerEntity.CustomerNumber = $"17-{recordNumber + 1}";
            }

            await _customerRepo.ModiRec(customerEntity);

            return new TransactionResult<CustomerDto>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}