﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Customers.Queries
{
    public class CustomerGetRecListCommand : IRequest<IEnumerable<CustomerListItem>>
    {
    }

    public class CustomerGetRecListHandler : IRequestHandler<CustomerGetRecListCommand, IEnumerable<CustomerListItem>>
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepo;

        public CustomerGetRecListHandler(IMapper mapper, ICustomerRepository customerRepo)
        {
            _mapper = mapper;
            _customerRepo = customerRepo;
        }

        public async Task<IEnumerable<CustomerListItem>> Handle(CustomerGetRecListCommand request, CancellationToken cancellationToken)
        {
            var customerList = await _customerRepo.CustomerGetRecList();
            return _mapper.Map<IEnumerable<CustomerListItem>>(customerList);
        }
    }
}