﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Customers.Queries
{
    public class CustomerGetRecCommand : IRequest<CustomerDto>
    {
        public int CustomerId { get; set; }
    }

    public class CustomerGetRecHandler : IRequestHandler<CustomerGetRecCommand, CustomerDto>
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepo;

        public CustomerGetRecHandler(IMapper mapper, ICustomerRepository customerRepo)
        {
            _mapper = mapper;
            _customerRepo = customerRepo;
        }

        public async Task<CustomerDto> Handle(CustomerGetRecCommand request, CancellationToken cancellationToken)
        {
            var customerEntity = await _customerRepo.CustomerGetRec(request.CustomerId);
            return _mapper.Map<CustomerDto>(customerEntity);
        }
    }
}