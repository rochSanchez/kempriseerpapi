﻿using AutoMapper;
using ERP.WebApi.Business.Services.ErpEmailService.Sales;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Inquiry.Commands
{
    public class InquiryAddRecCommand : IRequest<TransactionResult<InquiryAggregate>>
    {
        public InquiryAggregate InquiryAggregate { get; set; }
    }

    public class InquiryAddRecHandler : IRequestHandler<InquiryAddRecCommand, TransactionResult<InquiryAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IInquiryRepository _inquiryRepo;
        private readonly IInquiryEmailService _inquiryEmailService;

        public InquiryAddRecHandler(IMapper mapper, IInquiryRepository inquiryRepo, IInquiryEmailService inquiryEmailService)
        {
            _mapper = mapper;
            _inquiryRepo = inquiryRepo;
            _inquiryEmailService = inquiryEmailService;
        }

        public async Task<TransactionResult<InquiryAggregate>> Handle(InquiryAddRecCommand request,
            CancellationToken cancellationToken)
        {
            var inquiryDto = request.InquiryAggregate.InquiryDto;
            var inquiryDetails = request.InquiryAggregate.InquiryDetails;
            var inquiryFiles = request.InquiryAggregate.InquiryFiles;

            var inquiryEntity = _mapper.Map<Data.Entities.Inquiry>(inquiryDto);
            inquiryEntity.InquiryDetails = _mapper.Map<List<InquiryDetail>>(inquiryDetails);
            inquiryEntity.InquiryFiles = _mapper.Map<List<File>>(inquiryFiles);

            var latestRecord = await _inquiryRepo.GetLatestRecord();
            var addRecResult = await _inquiryRepo.AddRec(inquiryEntity);

            var sequence = await _inquiryRepo.GetSequence();
            inquiryEntity.Sequence = sequence;

            if (sequence == 1)
                inquiryEntity.InquiryNumber = "12-100001";
            else
            {
                var splitNumber = latestRecord.InquiryNumber.Split('-');
                var recordNumber = Convert.ToDecimal(splitNumber[1]);
                inquiryEntity.InquiryNumber = $"12-{recordNumber + 1}";
            }

            await _inquiryRepo.ModiRec(inquiryEntity);

            var inquiryId = inquiryEntity.InquiryId;
            inquiryEntity = await _inquiryRepo.InquiryGetRec(inquiryId);

            _inquiryEmailService.SendNewInquiryNotification(inquiryEntity);

            return new TransactionResult<InquiryAggregate>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}