﻿using AutoMapper;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Inquiry.Commands
{
    public class InquiryModiRecCommand : IRequest<TransactionResult<InquiryAggregate>>
    {
        public InquiryAggregate InquiryAggregate { get; set; }
    }

    public class InquiryModiRecHandler : IRequestHandler<InquiryModiRecCommand, TransactionResult<InquiryAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IInquiryRepository _inquiryRepo;
        private readonly IReasonRepository _reasonRepo;

        public InquiryModiRecHandler(IMapper mapper, IInquiryRepository inquiryRepo, IReasonRepository reasonRepo)
        {
            _inquiryRepo = inquiryRepo;
            _reasonRepo = reasonRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<InquiryAggregate>> Handle(InquiryModiRecCommand request, CancellationToken cancellationToken)
        {
            var inquiryDto = request.InquiryAggregate.InquiryDto;
            var inquiryDetails = request.InquiryAggregate.InquiryDetails;
            var inquiryFiles = request.InquiryAggregate.InquiryFiles;

            var inquiryEntity = _mapper.Map<Data.Entities.Inquiry>(inquiryDto);
            inquiryEntity.InquiryDetails = _mapper.Map<List<InquiryDetail>>(inquiryDetails);
            inquiryEntity.InquiryFiles = _mapper.Map<List<File>>(inquiryFiles);

            var modiRecResult = await _inquiryRepo.ModiRec(inquiryEntity);

            var reasonsEntity = await _reasonRepo.GetRecList();
            if (reasonsEntity.All(x => x.Name != inquiryEntity.ReasonForClosing))
                await _reasonRepo.AddRec(new Reason { Name = inquiryEntity.ReasonForClosing });

            return new TransactionResult<InquiryAggregate>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}