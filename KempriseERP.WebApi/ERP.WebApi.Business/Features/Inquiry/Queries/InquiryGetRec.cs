﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Inquiry.Queries
{
    public class InquiryGetRecCommand : IRequest<InquiryAggregate>
    {
        public int InquiryId { get; set; }
    }

    public class InquiryGetRecHandler : IRequestHandler<InquiryGetRecCommand, InquiryAggregate>
    {
        private readonly IMapper _mapper;
        private readonly IInquiryRepository _inquiryRepo;

        public InquiryGetRecHandler(IMapper mapper, IInquiryRepository inquiryRepo)
        {
            _mapper = mapper;
            _inquiryRepo = inquiryRepo;
        }

        public async Task<InquiryAggregate> Handle(InquiryGetRecCommand request, CancellationToken cancellationToken)
        {
            var inquiryEntity = await _inquiryRepo.InquiryGetRec(request.InquiryId);

            var aggregate = new InquiryAggregate
            {
                InquiryDto = _mapper.Map<InquiryDto>(inquiryEntity),
                InquiryDetails = _mapper.Map<List<InquiryDetailDto>>(inquiryEntity.InquiryDetails),
                InquiryFiles = _mapper.Map<List<FileDto>>(inquiryEntity.InquiryFiles)
            };

            foreach (var detail in aggregate.InquiryDetails)
            {
                if (string.IsNullOrEmpty(detail.PartNumber))
                    detail.PartNumber = "NPN";
            }

            return aggregate;
        }
    }
}