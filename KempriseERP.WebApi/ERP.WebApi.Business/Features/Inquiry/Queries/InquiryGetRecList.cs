﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Inquiry.Queries
{
    public class InquiryGetRecListCommand : IRequest<IEnumerable<InquiryListItem>>
    {
    }

    public class InquiryGetRecListHandler : IRequestHandler<InquiryGetRecListCommand, IEnumerable<InquiryListItem>>
    {
        private readonly IMapper _mapper;
        private readonly IInquiryRepository _inquiryRepo;

        public InquiryGetRecListHandler(IMapper mapper, IInquiryRepository inquiryRepo)
        {
            _mapper = mapper;
            _inquiryRepo = inquiryRepo;
        }

        public async Task<IEnumerable<InquiryListItem>> Handle(InquiryGetRecListCommand request, CancellationToken cancellationToken)
        {
            var inquiryList = await _inquiryRepo.InquiryGetRecList();
            return _mapper.Map<IEnumerable<InquiryListItem>>(inquiryList);
        }
    }
}