﻿using AutoMapper;
using ERP.WebApi.Business.Services.ErpEmailService.Sales;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Quotation.Commands
{
    public class QuotationAddRecCommand : IRequest<TransactionResult<QuotationAggregate>>
    {
        public QuotationAggregate QuotationAggregate { get; set; }
    }

    public class QuotationAddRecHandler : IRequestHandler<QuotationAddRecCommand, TransactionResult<QuotationAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IQuotationRepository _quotationRepo;
        private readonly IInquiryRepository _inquiryRepo;
        private readonly IQuotationEmailService _quotationEmailService;

        public QuotationAddRecHandler(IMapper mapper, IQuotationRepository quotationRepo, IInquiryRepository inquiryRepo,
            IQuotationEmailService quotationEmailService)
        {
            _mapper = mapper;
            _quotationRepo = quotationRepo;
            _inquiryRepo = inquiryRepo;
            _quotationEmailService = quotationEmailService;
        }

        public async Task<TransactionResult<QuotationAggregate>> Handle(QuotationAddRecCommand request, CancellationToken cancellationToken)
        {
            var quotationDto = request.QuotationAggregate.QuotationDto;
            var quotationDetails = request.QuotationAggregate.QuotationDetails;
            foreach (var detail in quotationDetails)
            {
                detail.QuotationId = 0;
                detail.QuotationDetailId = 0;
            }
            var quotationNotes = request.QuotationAggregate.QuotationNotes;
            var quotationFiles = request.QuotationAggregate.QuotationFiles;

            var newNotes = quotationNotes.Where(x => x.NoteId == 0).ToList();
            var newFiles = quotationFiles.Where(x => x.FileId == 0).ToList();

            var quotation = _mapper.Map<Data.Entities.Quotation>(quotationDto);
            quotation.QuotationDetails = _mapper.Map<List<QuotationDetail>>(quotationDetails);
            quotation.QuotationNotes = _mapper.Map<List<Note>>(newNotes);
            quotation.QuotationFiles = _mapper.Map<List<File>>(newFiles);

            if (quotation.QuotationId != 0)
            {
                var quotationEntity = await _quotationRepo.QuotationGetRec(quotation.QuotationId);
                quotationEntity.Status = QuotationStatus.Closed;
                quotationEntity.ReasonForClosing = "REVISED";
                await _quotationRepo.ModiRec(quotationEntity);

                quotation.QuotationId = 0;
            }

            var addRecResult = await _quotationRepo.AddRec(quotation);

            var sequence = await _quotationRepo.GetSequence();
            quotation.Sequence = sequence;

            var versionCount = await _quotationRepo.GetVersionCount(quotation.QuotationNumber);
            quotation.Version = versionCount;

            var oldNotes = quotationNotes.Where(x => x.NoteId != 0).ToList();
            var notesEntity = _mapper.Map<List<Note>>(oldNotes);
            notesEntity.ForEach(quotation.QuotationNotes.Add);

            var oldFiles = quotationFiles.Where(x => x.FileId != 0).ToList();
            var filesEntity = _mapper.Map<List<File>>(oldFiles);
            filesEntity.ForEach(quotation.QuotationFiles.Add);

            await _quotationRepo.ModiRec(quotation);

            var inquiryEntity = await _inquiryRepo.InquiryGetRec(quotation.InquiryId);
            inquiryEntity.Status = InquiryStatus.QuotationCreated;

            await _inquiryRepo.ModiRec(inquiryEntity);

            var quotationId = quotation.QuotationId;
            quotation = await _quotationRepo.QuotationGetRec(quotationId);

            switch (quotation.Status)
            {
                case QuotationStatus.SubmittedForChecking:
                    _quotationEmailService.SendCheckQuotationNotification(quotation);
                    break;

                case QuotationStatus.Approved:
                    _quotationEmailService.SendQuotationApprovedNotification(quotation);
                    break;
            }

            return new TransactionResult<QuotationAggregate>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}