﻿using AutoMapper;
using ERP.WebApi.Business.Services.ErpEmailService.Sales;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Quotation.Commands
{
    public class QuotationModiRecCommand : IRequest<TransactionResult<QuotationAggregate>>
    {
        public QuotationAggregate QuotationAggregate { get; set; }
    }

    public class QuotationModiRecHandler : IRequestHandler<QuotationModiRecCommand, TransactionResult<QuotationAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IQuotationRepository _quotationRepo;
        private readonly IReasonRepository _reasonRepo;
        private readonly IQuotationEmailService _quotationEmailService;

        public QuotationModiRecHandler(IMapper mapper, IQuotationRepository quotationRepo, IReasonRepository reasonRepo,
            IQuotationEmailService quotationEmailService)
        {
            _quotationRepo = quotationRepo;
            _reasonRepo = reasonRepo;
            _quotationEmailService = quotationEmailService;
            _mapper = mapper;
        }

        public async Task<TransactionResult<QuotationAggregate>> Handle(QuotationModiRecCommand request, CancellationToken cancellationToken)
        {
            var quotationDto = request.QuotationAggregate.QuotationDto;
            var quotationDetails = request.QuotationAggregate.QuotationDetails;
            var quotationNotes = request.QuotationAggregate.QuotationNotes;
            var quotationFiles = request.QuotationAggregate.QuotationFiles;

            var quotationEntity = _mapper.Map<Data.Entities.Quotation>(quotationDto);
            quotationEntity.QuotationDetails = _mapper.Map<List<QuotationDetail>>(quotationDetails);
            quotationEntity.QuotationNotes = _mapper.Map<List<Note>>(quotationNotes);
            quotationEntity.QuotationFiles = _mapper.Map<List<File>>(quotationFiles);

            var modiRecResult = await _quotationRepo.ModiRec(quotationEntity);

            var reasonsEntity = await _reasonRepo.GetRecList();
            if (reasonsEntity.All(x => x.Name != quotationEntity.ReasonForClosing))
                await _reasonRepo.AddRec(new Reason { Name = quotationEntity.ReasonForClosing });

            var quotationId = quotationEntity.QuotationId;
            quotationEntity = await _quotationRepo.QuotationGetRec(quotationId);

            switch (quotationEntity.Status)
            {
                case QuotationStatus.RevisionRequested:
                    _quotationEmailService.SendReviseQuotationNotification(quotationEntity);
                    break;

                case QuotationStatus.Closed:
                    _quotationEmailService.SendCloseQuotationNotification(quotationEntity);
                    break;

                case QuotationStatus.SubmittedForApproval:
                    _quotationEmailService.SendApprovalQuotationNotification(quotationEntity);
                    break;

                case QuotationStatus.Approved:
                    _quotationEmailService.SendQuotationApprovedNotification(quotationEntity);
                    break;

                case QuotationStatus.SalesOrder:
                    _quotationEmailService.SendOrderReceivedNotification(quotationEntity);
                    break;
            }

            return new TransactionResult<QuotationAggregate>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}