﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Quotation.Queries
{
    public class QuotationGetRecCommand : IRequest<QuotationAggregate>
    {
        public int QuotationId { get; set; }
    }

    public class QuotationGetRecHandler : IRequestHandler<QuotationGetRecCommand, QuotationAggregate>
    {
        private readonly IMapper _mapper;
        private readonly IQuotationRepository _quotationRepo;

        public QuotationGetRecHandler(IMapper mapper, IQuotationRepository quotationRepo)
        {
            _mapper = mapper;
            _quotationRepo = quotationRepo;
        }

        public async Task<QuotationAggregate> Handle(QuotationGetRecCommand request, CancellationToken cancellationToken)
        {
            var quotationEntity = await _quotationRepo.QuotationGetRec(request.QuotationId);

            var aggregate = new QuotationAggregate
            {
                QuotationDto = _mapper.Map<QuotationDto>(quotationEntity),
                QuotationDetails = _mapper.Map<List<QuotationDetailDto>>(quotationEntity.QuotationDetails),
                QuotationNotes = _mapper.Map<List<NoteDto>>(quotationEntity.QuotationNotes),
                QuotationFiles = _mapper.Map<List<FileDto>>(quotationEntity.QuotationFiles)
            };

            foreach (var detail in aggregate.QuotationDetails)
            {
                if (string.IsNullOrEmpty(detail.PartNumber))
                    detail.PartNumber = "NPN";
            }

            return aggregate;
        }
    }
}