﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Quotation.Queries
{
    public class QuotationGetRecListCommand : IRequest<IEnumerable<QuotationListItem>>
    {
    }

    public class QuotationGetRecListHandler : IRequestHandler<QuotationGetRecListCommand, IEnumerable<QuotationListItem>>
    {
        private readonly IMapper _mapper;
        private readonly IQuotationRepository _quotationRepo;

        public QuotationGetRecListHandler(IMapper mapper, IQuotationRepository quotationRepo)
        {
            _mapper = mapper;
            _quotationRepo = quotationRepo;
        }

        public async Task<IEnumerable<QuotationListItem>> Handle(QuotationGetRecListCommand request, CancellationToken cancellationToken)
        {
            var quotationList = await _quotationRepo.QuotationGetRecList();
            return _mapper.Map<IEnumerable<QuotationListItem>>(quotationList);
        }
    }
}