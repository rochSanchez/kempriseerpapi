﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using ERP.WebApi.DTO.Security.Contracts;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Lookups
{
    public class SalesGetLookupsCommand : IRequest<SalesLookupAggregate>
    {
    }

    public class SalesGetLookupsHandler : IRequestHandler<SalesGetLookupsCommand, SalesLookupAggregate>
    {
        private readonly IMapper _mapper;
        private readonly ICustomerRepository _customerRepo;
        private readonly ICurrencyRepository _currencyRepo;
        private readonly ICategoryRepository _categoryRepo;
        private readonly IPartRepository _partRepo;
        private readonly IUserManagementService _userManagementService;
        private readonly IReasonRepository _reasonRepo;

        public SalesGetLookupsHandler(IMapper mapper, ICustomerRepository customerRepo, ICurrencyRepository currencyRepo,
            ICategoryRepository categoryRepo, IPartRepository partRepo, IUserManagementService userManagementService,
            IReasonRepository reasonRepo)
        {
            _mapper = mapper;
            _customerRepo = customerRepo;
            _currencyRepo = currencyRepo;
            _categoryRepo = categoryRepo;
            _partRepo = partRepo;
            _userManagementService = userManagementService;
            _reasonRepo = reasonRepo;
        }

        public async Task<SalesLookupAggregate> Handle(SalesGetLookupsCommand request, CancellationToken cancellationToken)
        {
            var customerList = await _customerRepo.CustomerGetRecList();
            var currencyList = await _currencyRepo.GetRecList();
            var categoryList = await _categoryRepo.GetRecList();
            var partList = await _partRepo.GetRecList();
            var salesPersonList = await _userManagementService.GetUserByRoleCategory(RoleCategory.Sales);
            var reasonList = await _reasonRepo.GetRecList();

            return new SalesLookupAggregate
            {
                Customers = _mapper.Map<List<CustomerLookup>>(customerList.OrderByDescending(x => x.CustomerNumber)),
                Currencies = _mapper.Map<List<CurrencyLookup>>(currencyList),
                Categories = _mapper.Map<List<CategoryLookup>>(categoryList.OrderBy(x => x.CategoryName)),
                Parts = _mapper.Map<List<PartLookup>>(partList.OrderByDescending(x => x.PartNumber)),
                SalesPersons = _mapper.Map<List<SalesPersonLookup>>(salesPersonList.OrderByDescending(x => x.LastName)),
                Reasons = _mapper.Map<List<ReasonLookup>>(reasonList)
            };
        }
    }
}