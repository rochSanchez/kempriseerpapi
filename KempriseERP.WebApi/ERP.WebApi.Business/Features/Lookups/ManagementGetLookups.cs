﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.Lookups.LookupAggregates;
using ERP.WebApi.DTO.Lookups.LookupItems;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Lookups
{
    public class ManagementGetLookupsCommand : IRequest<ManagementLookupAggregate>
    {
    }

    public class ManagementGetLookupsHandler : IRequestHandler<ManagementGetLookupsCommand, ManagementLookupAggregate>
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepo;

        public ManagementGetLookupsHandler(IMapper mapper, ICompanyRepository companyRepo)
        {
            _mapper = mapper;
            _companyRepo = companyRepo;
        }

        public async Task<ManagementLookupAggregate> Handle(ManagementGetLookupsCommand request, CancellationToken cancellationToken)
        {
            var companies = await _companyRepo.GetRecList();

            return new ManagementLookupAggregate
            {
                Companies = _mapper.Map<List<CompanyLookup>>(companies)
            };
        }
    }
}