﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Supplier.Commands
{
    public class SupplierModiRecCommand : IRequest<TransactionResult<SupplierDto>>
    {
        public SupplierDto SupplierDto { get; set; }
    }

    public class SupplierModiRecHandler : IRequestHandler<SupplierModiRecCommand, TransactionResult<SupplierDto>>
    {
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepo;

        public SupplierModiRecHandler(IMapper mapper, ISupplierRepository supplierRepo)
        {
            _supplierRepo = supplierRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<SupplierDto>> Handle(SupplierModiRecCommand request, CancellationToken cancellationToken)
        {
            var dataToSave = _mapper.Map<Data.Entities.Supplier>(request.SupplierDto);
            var modiRecResult = await _supplierRepo.ModiRec(dataToSave);

            return new TransactionResult<SupplierDto>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}