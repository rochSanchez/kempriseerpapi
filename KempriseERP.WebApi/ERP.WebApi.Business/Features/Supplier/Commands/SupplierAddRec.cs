﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Supplier.Commands
{
    public class SupplierAddRecCommand : IRequest<TransactionResult<SupplierDto>>
    {
        public SupplierDto SupplierDto { get; set; }
    }

    public class SupplierAddRecHandler : IRequestHandler<SupplierAddRecCommand, TransactionResult<SupplierDto>>
    {
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepo;

        public SupplierAddRecHandler(IMapper mapper, ISupplierRepository supplierRepo)
        {
            _mapper = mapper;
            _supplierRepo = supplierRepo;
        }

        public async Task<TransactionResult<SupplierDto>> Handle(SupplierAddRecCommand request, CancellationToken cancellationToken)
        {
            var supplierEntity = _mapper.Map<Data.Entities.Supplier>(request.SupplierDto);

            var latestRecord = await _supplierRepo.GetLatestRecord();
            var addRecResult = await _supplierRepo.AddRec(supplierEntity);

            var sequence = await _supplierRepo.GetSequence();
            supplierEntity.Sequence = sequence;

            if (sequence == 1)
                supplierEntity.SupplierNumber = "19-1001";
            else
            {
                var splitNumber = latestRecord.SupplierNumber.Split('-');
                var recordNumber = Convert.ToDecimal(splitNumber[1]);
                supplierEntity.SupplierNumber = $"19-{recordNumber + 1}";
            }

            await _supplierRepo.ModiRec(supplierEntity);

            return new TransactionResult<SupplierDto>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}