﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Supplier.Queries
{
    public class SupplierGetRecListCommand : IRequest<IEnumerable<SupplierListItem>>
    {
    }

    public class SupplierGetRecListHandler : IRequestHandler<SupplierGetRecListCommand, IEnumerable<SupplierListItem>>
    {
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepo;

        public SupplierGetRecListHandler(IMapper mapper, ISupplierRepository supplierRepo)
        {
            _mapper = mapper;
            _supplierRepo = supplierRepo;
        }

        public async Task<IEnumerable<SupplierListItem>> Handle(SupplierGetRecListCommand request, CancellationToken cancellationToken)
        {
            var supplierList = await _supplierRepo.GetRecList();
            return _mapper.Map<IEnumerable<SupplierListItem>>(supplierList);
        }
    }
}