﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Supplier.Queries
{
    public class SupplierGetRecCommand : IRequest<SupplierDto>
    {
        public int SupplierId { get; set; }
    }

    public class SupplierGetRecHandler : IRequestHandler<SupplierGetRecCommand, SupplierDto>
    {
        private readonly IMapper _mapper;
        private readonly ISupplierRepository _supplierRepo;

        public SupplierGetRecHandler(IMapper mapper, ISupplierRepository supplierRepo)
        {
            _mapper = mapper;
            _supplierRepo = supplierRepo;
        }

        public async Task<SupplierDto> Handle(SupplierGetRecCommand request, CancellationToken cancellationToken)
        {
            var supplierEntity = await _supplierRepo.GetRec(request.SupplierId);
            return _mapper.Map<SupplierDto>(supplierEntity);
        }
    }
}