﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Currency.Queries
{
    public class CurrencyGetRecListCommand : IRequest<IEnumerable<CurrencyListItem>>
    {
    }

    public class CurrencyGetRecListHandler : IRequestHandler<CurrencyGetRecListCommand, IEnumerable<CurrencyListItem>>
    {
        private readonly IMapper _mapper;
        private readonly ICurrencyRepository _currencyRepo;

        public CurrencyGetRecListHandler(IMapper mapper, ICurrencyRepository currencyRepo)
        {
            _mapper = mapper;
            _currencyRepo = currencyRepo;
        }

        public async Task<IEnumerable<CurrencyListItem>> Handle(CurrencyGetRecListCommand request, CancellationToken cancellationToken)
        {
            var currencyList = await _currencyRepo.GetRecList();
            return _mapper.Map<IEnumerable<CurrencyListItem>>(currencyList);
        }
    }
}