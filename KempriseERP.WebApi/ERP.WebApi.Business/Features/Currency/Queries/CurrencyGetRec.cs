﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Currency.Queries
{
    public class CurrencyGetRecCommand : IRequest<CurrencyDto>
    {
        public int CurrencyId { get; set; }
    }

    public class CurrencyGetRecHandler : IRequestHandler<CurrencyGetRecCommand, CurrencyDto>
    {
        private readonly IMapper _mapper;
        private readonly ICurrencyRepository _currencyRepo;

        public CurrencyGetRecHandler(IMapper mapper, ICurrencyRepository currencyRepo)
        {
            _mapper = mapper;
            _currencyRepo = currencyRepo;
        }

        public async Task<CurrencyDto> Handle(CurrencyGetRecCommand request, CancellationToken cancellationToken)
        {
            var currencyEntity = await _currencyRepo.GetRec(request.CurrencyId);
            return _mapper.Map<CurrencyDto>(currencyEntity);
        }
    }
}