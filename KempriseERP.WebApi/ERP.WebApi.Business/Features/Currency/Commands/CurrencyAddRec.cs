﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Currency.Commands
{
    public class CurrencyAddRecCommand : IRequest<TransactionResult<CurrencyDto>>
    {
        public CurrencyDto CurrencyDto { get; set; }
    }

    public class CurrencyAddRecHandler : IRequestHandler<CurrencyAddRecCommand, TransactionResult<CurrencyDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICurrencyRepository _currencyRepo;

        public CurrencyAddRecHandler(IMapper mapper, ICurrencyRepository currencyRepo)
        {
            _mapper = mapper;
            _currencyRepo = currencyRepo;
        }

        public async Task<TransactionResult<CurrencyDto>> Handle(CurrencyAddRecCommand request, CancellationToken cancellationToken)
        {
            var currency = _mapper.Map<Data.Entities.Currency>(request.CurrencyDto);
            var addRecResult = await _currencyRepo.AddRec(currency);

            var sequence = await _currencyRepo.GetSequence();
            currency.Sequence = sequence;

            await _currencyRepo.ModiRec(currency);

            return new TransactionResult<CurrencyDto>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}