﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Currency.Commands
{
    public class CurrencyModiRecCommand : IRequest<TransactionResult<CurrencyDto>>
    {
        public CurrencyDto CurrencyDto { get; set; }
    }

    public class CurrencyModiRecHandler : IRequestHandler<CurrencyModiRecCommand, TransactionResult<CurrencyDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICurrencyRepository _currencyRepo;

        public CurrencyModiRecHandler(IMapper mapper, ICurrencyRepository currencyRepo)
        {
            _currencyRepo = currencyRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<CurrencyDto>> Handle(CurrencyModiRecCommand request, CancellationToken cancellationToken)
        {
            var dataToSave = _mapper.Map<Data.Entities.Currency>(request.CurrencyDto);
            var modiRecResult = await _currencyRepo.ModiRec(dataToSave);

            return new TransactionResult<CurrencyDto>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}