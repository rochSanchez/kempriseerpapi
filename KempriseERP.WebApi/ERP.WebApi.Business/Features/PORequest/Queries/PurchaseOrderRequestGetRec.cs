﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Procurement.Request;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;

namespace ERP.WebApi.Business.Features.PORequest.Queries
{
    public class PurchaseOrderRequestGetRecCommand : IRequest<PoRequestAggregate>
    {
        public int PuchaseRequestId { get; set; }
    }

    public class PurchaseOrderGetRecHandler : IRequestHandler<PurchaseOrderRequestGetRecCommand, PoRequestAggregate>
    {
        private readonly IMapper _mapper;
        private readonly IPurchaseRequestRepository _purchaseRequestRepository;

        public PurchaseOrderGetRecHandler(IMapper mapper, IPurchaseRequestRepository purchaseRequestRepository)
        {
            _mapper = mapper;
            _purchaseRequestRepository = purchaseRequestRepository;
        }

        public async Task<PoRequestAggregate> Handle(PurchaseOrderRequestGetRecCommand request, CancellationToken cancellationToken)
        {
            var inquiryEntity = await _purchaseRequestRepository.PurchaseRequestGetRec(request.PuchaseRequestId);

            var aggregate = new PoRequestAggregate();

            return aggregate;
        }
    }
}