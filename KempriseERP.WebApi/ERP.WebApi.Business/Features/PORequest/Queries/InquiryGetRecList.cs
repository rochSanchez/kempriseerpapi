﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Procurement.Request;
using MediatR;

namespace ERP.WebApi.Business.Features.PORequest.Queries
{
    public class PurchaseOrderRequestGetRecListCommand : IRequest<IEnumerable<PurchaseOrderRequestListItem>>
    {
    }

    public class InquiryGetRecListHandler : IRequestHandler<PurchaseOrderRequestGetRecListCommand, IEnumerable<PurchaseOrderRequestListItem>>
    {
        private readonly IMapper _mapper;
        private readonly IPurchaseRequestRepository _purchaseRequestRepository;

        public InquiryGetRecListHandler(IMapper mapper, IPurchaseRequestRepository purchaseRequestRepository)
        {
            _mapper = mapper;
            _purchaseRequestRepository = purchaseRequestRepository;
        }

        public async Task<IEnumerable<PurchaseOrderRequestListItem>> Handle(PurchaseOrderRequestGetRecListCommand request, CancellationToken cancellationToken)
        {
            var purchaseRequests = await _purchaseRequestRepository.PurchaseRequestGetRecList();
            return _mapper.Map<IEnumerable<PurchaseOrderRequestListItem>>(purchaseRequests);
        }
    }
}