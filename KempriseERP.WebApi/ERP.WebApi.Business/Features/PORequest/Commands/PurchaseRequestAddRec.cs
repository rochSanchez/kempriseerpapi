﻿using AutoMapper;
using ERP.WebApi.Business.Services.ErpEmailService.Sales;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Procurement.Request;

namespace ERP.WebApi.Business.Features.PORequest.Commands
{
    public class PurchaseRequestAddRecCommand : IRequest<TransactionResult<PoRequestAggregate>>
    {
        public InquiryAggregate InquiryAggregate { get; set; }
    }

    public class PurchaseRequestAddRecHandler : IRequestHandler<PurchaseRequestAddRecCommand, TransactionResult<PoRequestAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IPurchaseRequestRepository _purchaseRequestRepository;
        private readonly IInquiryEmailService _inquiryEmailService;

        public PurchaseRequestAddRecHandler(IMapper mapper, IPurchaseRequestRepository purchaseRequestRepository, IInquiryEmailService inquiryEmailService)
        {
            _mapper = mapper;
            _purchaseRequestRepository = purchaseRequestRepository;
            _inquiryEmailService = inquiryEmailService;
        }

        public async Task<TransactionResult<PoRequestAggregate>> Handle(PurchaseRequestAddRecCommand request,
            CancellationToken cancellationToken)
        {
            return new TransactionResult<PoRequestAggregate>
            {
        
            };
        }
    }
}