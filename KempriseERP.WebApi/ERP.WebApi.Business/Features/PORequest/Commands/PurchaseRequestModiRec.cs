﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Procurement.Request;
using MediatR;

namespace ERP.WebApi.Business.Features.PORequest.Commands
{
    public class PurchaseRequestModiRecCommand : IRequest<TransactionResult<PoRequestAggregate>>
    {
        public PoRequestAggregate PoRequestAggregate { get; set; }
    }

    public class PurchaseRequestModiRecHandler : IRequestHandler<PurchaseRequestModiRecCommand, TransactionResult<PoRequestAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IPurchaseRequestRepository _purchaseRequestRepository;
        private readonly IReasonRepository _reasonRepo;

        public PurchaseRequestModiRecHandler(IMapper mapper, IPurchaseRequestRepository purchaseRequestRepository, IReasonRepository reasonRepo)
        {
            _purchaseRequestRepository = purchaseRequestRepository;
            _reasonRepo = reasonRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<PoRequestAggregate>> Handle(PurchaseRequestModiRecCommand request, CancellationToken cancellationToken)
        {

            return new TransactionResult<PoRequestAggregate>
            {
            };
        }
    }
}