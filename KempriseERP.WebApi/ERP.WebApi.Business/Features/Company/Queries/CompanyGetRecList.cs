﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Company;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Company.Queries
{
    public class CompanyGetRecListCommand : IRequest<IEnumerable<CompanyListItem>>
    {
    }

    public class CompanyGetRecListHandler : IRequestHandler<CompanyGetRecListCommand, IEnumerable<CompanyListItem>>
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepo;

        public CompanyGetRecListHandler(IMapper mapper, ICompanyRepository companyRepo)
        {
            _mapper = mapper;
            _companyRepo = companyRepo;
        }

        public async Task<IEnumerable<CompanyListItem>> Handle(CompanyGetRecListCommand request, CancellationToken cancellationToken)
        {
            var companyList = await _companyRepo.GetRecList();
            return _mapper.Map<IEnumerable<CompanyListItem>>(companyList);
        }
    }
}