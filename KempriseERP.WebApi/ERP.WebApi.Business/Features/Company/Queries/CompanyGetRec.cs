﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Company;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Company.Queries
{
    public class CompanyGetRecCommand : IRequest<CompanyDto>
    {
        public int CompanyId { get; set; }
    }

    public class CompanyGetRecHandler : IRequestHandler<CompanyGetRecCommand, CompanyDto>
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepo;

        public CompanyGetRecHandler(IMapper mapper, ICompanyRepository companyRepo)
        {
            _mapper = mapper;
            _companyRepo = companyRepo;
        }

        public async Task<CompanyDto> Handle(CompanyGetRecCommand request, CancellationToken cancellationToken)
        {
            var companyEntity = await _companyRepo.GetRec(request.CompanyId);
            return _mapper.Map<CompanyDto>(companyEntity);
        }
    }
}