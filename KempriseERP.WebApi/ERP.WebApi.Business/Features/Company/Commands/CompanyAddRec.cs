﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Company;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Company.Commands
{
    public class CompanyAddRecCommand : IRequest<TransactionResult<CompanyDto>>
    {
        public CompanyDto CompanyDto { get; set; }
    }

    public class CompanyAddRecHandler : IRequestHandler<CompanyAddRecCommand, TransactionResult<CompanyDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepo;

        public CompanyAddRecHandler(IMapper mapper, ICompanyRepository companyRepo)
        {
            _mapper = mapper;
            _companyRepo = companyRepo;
        }

        public async Task<TransactionResult<CompanyDto>> Handle(CompanyAddRecCommand request, CancellationToken cancellationToken)
        {
            var company = _mapper.Map<Data.Entities.Company>(request.CompanyDto);
            var addRecResult = await _companyRepo.AddRec(company);

            var sequence = await _companyRepo.GetSequence();
            company.Sequence = sequence;

            await _companyRepo.ModiRec(company);

            return new TransactionResult<CompanyDto>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}