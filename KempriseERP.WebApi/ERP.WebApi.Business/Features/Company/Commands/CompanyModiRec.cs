﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Company;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Company.Commands
{
    public class CompanyModiRecCommand : IRequest<TransactionResult<CompanyDto>>
    {
        public CompanyDto CompanyDto { get; set; }
    }

    public class CompanyModiRecHandler : IRequestHandler<CompanyModiRecCommand, TransactionResult<CompanyDto>>
    {
        private readonly IMapper _mapper;
        private readonly ICompanyRepository _companyRepo;

        public CompanyModiRecHandler(IMapper mapper, ICompanyRepository companyRepo)
        {
            _companyRepo = companyRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<CompanyDto>> Handle(CompanyModiRecCommand request, CancellationToken cancellationToken)
        {
            var dataToSave = _mapper.Map<Data.Entities.Company>(request.CompanyDto);
            var modiRecResult = await _companyRepo.ModiRec(dataToSave);

            return new TransactionResult<CompanyDto>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}