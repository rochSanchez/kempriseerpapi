﻿using System.Collections.Generic;

namespace ERP.WebApi.Business.Features
{
    public class TransactionResult<T> where T : class
    {
        public TransactionResult()
        {
            IsSuccessful = true;
        }

        public bool IsSuccessful { get; set; }

        public T Entity { get; set; }

        public string Message { get; set; }

        public List<string> ValidationErrors { get; set; }
    }
}