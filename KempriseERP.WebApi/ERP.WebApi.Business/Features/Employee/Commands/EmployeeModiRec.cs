﻿using AutoMapper;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Employee.Commands
{
    public class EmployeeModiRecCommand : IRequest<TransactionResult<EmployeeAggregate>>
    {
        public EmployeeAggregate EmployeeAggregate { get; set; }
    }

    public class EmployeeModiRecHandler : IRequestHandler<EmployeeModiRecCommand, TransactionResult<EmployeeAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepo;

        public EmployeeModiRecHandler(IMapper mapper, IEmployeeRepository employeeRepo)
        {
            _employeeRepo = employeeRepo;
            _mapper = mapper;
        }

        public async Task<TransactionResult<EmployeeAggregate>> Handle(EmployeeModiRecCommand request, CancellationToken cancellationToken)
        {
            var employeeDto = request.EmployeeAggregate.EmployeeDto;
            var employeeFiles = request.EmployeeAggregate.EmployeeFiles;

            var employeeEntity = _mapper.Map<Data.Entities.Employee>(employeeDto);
            employeeEntity.EmployeeFiles = _mapper.Map<List<File>>(employeeFiles);

            var modiRecResult = await _employeeRepo.ModiRec(employeeEntity);

            return new TransactionResult<EmployeeAggregate>
            {
                IsSuccessful = modiRecResult
            };
        }
    }
}