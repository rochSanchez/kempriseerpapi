﻿using AutoMapper;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Employee.Commands
{
    public class EmployeeAddRecCommand : IRequest<TransactionResult<EmployeeAggregate>>
    {
        public EmployeeAggregate EmployeeAggregate { get; set; }
    }

    public class EmployeeAddRecHandler : IRequestHandler<EmployeeAddRecCommand, TransactionResult<EmployeeAggregate>>
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepo;

        public EmployeeAddRecHandler(IMapper mapper, IEmployeeRepository employeeRepo)
        {
            _mapper = mapper;
            _employeeRepo = employeeRepo;
        }

        public async Task<TransactionResult<EmployeeAggregate>> Handle(EmployeeAddRecCommand request, CancellationToken cancellationToken)
        {
            var employeeDto = request.EmployeeAggregate.EmployeeDto;
            var employeeFiles = request.EmployeeAggregate.EmployeeFiles;

            var employeeEntity = _mapper.Map<Data.Entities.Employee>(employeeDto);
            employeeEntity.EmployeeFiles = _mapper.Map<List<File>>(employeeFiles);

            var latestRecord = await _employeeRepo.GetLatestRecord();
            var addRecResult = await _employeeRepo.AddRec(employeeEntity);

            var sequence = await _employeeRepo.GetSequence();
            employeeEntity.Sequence = sequence;

            if (sequence == 1)
                employeeEntity.EmployeeNumber = "18-1001";
            else
            {
                var splitNumber = latestRecord.EmployeeNumber.Split('-');
                var recordNumber = Convert.ToDecimal(splitNumber[1]);
                employeeEntity.EmployeeNumber = $"18-{recordNumber + 1}";
            }
            await _employeeRepo.ModiRec(employeeEntity);

            return new TransactionResult<EmployeeAggregate>
            {
                IsSuccessful = addRecResult
            };
        }
    }
}