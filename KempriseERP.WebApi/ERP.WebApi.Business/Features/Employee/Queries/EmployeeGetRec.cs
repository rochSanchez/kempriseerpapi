﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Employee.Queries
{
    public class EmployeeGetRecCommand : IRequest<EmployeeAggregate>
    {
        public int EmployeeId { get; set; }
    }

    public class EmployeeGetRecHandler : IRequestHandler<EmployeeGetRecCommand, EmployeeAggregate>
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepo;

        public EmployeeGetRecHandler(IMapper mapper, IEmployeeRepository employeeRepo)
        {
            _mapper = mapper;
            _employeeRepo = employeeRepo;
        }

        public async Task<EmployeeAggregate> Handle(EmployeeGetRecCommand request, CancellationToken cancellationToken)
        {
            var employeeEntity = await _employeeRepo.EmployeeGetRec(request.EmployeeId);

            return new EmployeeAggregate
            {
                EmployeeDto = _mapper.Map<EmployeeDto>(employeeEntity),
                EmployeeFiles = _mapper.Map<List<FileDto>>(employeeEntity.EmployeeFiles)
            };
        }
    }
}