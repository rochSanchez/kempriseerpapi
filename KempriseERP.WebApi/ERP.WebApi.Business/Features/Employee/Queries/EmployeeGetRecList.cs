﻿using AutoMapper;
using ERP.WebApi.Data.Repository;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Features.Employee.Queries
{
    public class EmployeeGetRecListCommand : IRequest<IEnumerable<EmployeeListItem>>
    {
    }

    public class EmployeeGetRecListHandler : IRequestHandler<EmployeeGetRecListCommand, IEnumerable<EmployeeListItem>>
    {
        private readonly IMapper _mapper;
        private readonly IEmployeeRepository _employeeRepo;

        public EmployeeGetRecListHandler(IMapper mapper, IEmployeeRepository employeeRepo)
        {
            _mapper = mapper;
            _employeeRepo = employeeRepo;
        }

        public async Task<IEnumerable<EmployeeListItem>> Handle(EmployeeGetRecListCommand request, CancellationToken cancellationToken)
        {
            var employeeList = await _employeeRepo.GetRecList();
            return _mapper.Map<IEnumerable<EmployeeListItem>>(employeeList);
        }
    }
}