﻿using System;

namespace ERP.WebApi.Business.Exceptions
{
    public class NotFoundException : ApplicationException
    {
        public NotFoundException(string name, object key)
            : base($"{name} ({key}) is Not Found")
        {
        }
    }
}