﻿using AutoMapper;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.DTOs.Independent;
using ERP.WebApi.DTO.DTOs.Management.Company;
using ERP.WebApi.DTO.DTOs.Management.Currency;
using ERP.WebApi.DTO.DTOs.Management.Customer;
using ERP.WebApi.DTO.DTOs.Management.Employee;
using ERP.WebApi.DTO.DTOs.Management.Supplier;
using ERP.WebApi.DTO.DTOs.PLM.Category;
using ERP.WebApi.DTO.DTOs.PLM.Part;
using ERP.WebApi.DTO.DTOs.Sales.Inquiry;
using ERP.WebApi.DTO.DTOs.Sales.Quotation;
using ERP.WebApi.DTO.Lookups.LookupItems;

namespace ERP.WebApi.Business.MappingProfiles
{
    public class ErpMappings : Profile
    {
        public ErpMappings()
        {
            CreateMap<FileDto, File>().ReverseMap();
            CreateMap<NoteDto, Note>().ReverseMap();

            CreateMap<CustomerDto, Customer>(MemberList.Destination)
                .ForMember(dest => dest.CompanyId, opt =>
                {
                    opt.PreCondition(src => src.CompanyId != 0);
                    opt.MapFrom(src => src.CompanyId);
                });
            CreateMap<Customer, CustomerDto>(MemberList.Source)
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.CompanyAddress, opt => opt.MapFrom(src => src.Company.CompanyAddress))
                .ForMember(dest => dest.OfficeNumber, opt => opt.MapFrom(src => src.Company.OfficeNumber))
                .ForMember(dest => dest.Website, opt => opt.MapFrom(src => src.Company.Website));
            CreateMap<Customer, CustomerListItem>(MemberList.Destination)
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.OfficeNumber, opt => opt.MapFrom(src => src.Company.OfficeNumber))
                .ForMember(dest => dest.Website, opt => opt.MapFrom(src => src.Company.Website));
            CreateMap<Customer, CustomerLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CustomerId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.CompanyAddress, opt => opt.MapFrom(src => src.Company.CompanyAddress));

            CreateMap<CompanyDto, Company>().ReverseMap();
            CreateMap<Company, CompanyListItem>(MemberList.Destination);
            CreateMap<Company, CompanyLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CompanyId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));

            CreateMap<EmployeeDto, Employee>().ReverseMap();
            CreateMap<Employee, EmployeeListItem>(MemberList.Destination);

            CreateMap<SupplierDto, Supplier>().ReverseMap();
            CreateMap<Supplier, SupplierListItem>(MemberList.Destination);

            CreateMap<CurrencyDto, Currency>().ReverseMap();
            CreateMap<Currency, CurrencyListItem>(MemberList.Destination);
            CreateMap<Currency, CurrencyLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CurrencyId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Code));

            CreateMap<Inquiry, InquiryDto>()
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Customer.Company.Name))
                .ForMember(dest => dest.CompanyAddress, opt => opt.MapFrom(src => src.Customer.Company.CompanyAddress))
                .ForMember(dest => dest.CurrencyName, opt => opt.MapFrom(src => src.Currency.Code));
            CreateMap<InquiryDto, Inquiry>(MemberList.Destination);
            CreateMap<Inquiry, InquiryListItem>(MemberList.Destination)
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                .ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Customer.Company.Name))
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(src => src.Currency.Code))
                .ForMember(dest => dest.CreatedByFullName, opt => opt.MapFrom(src => src.CreatedByFullName))
                .ForMember(dest => dest.UpdatedByFullName, opt => opt.MapFrom(src => src.UpdatedByFullName));

            CreateMap<InquiryDetail, InquiryDetailDto>()
                .ForMember(dest => dest.PartNumber, opt => opt.MapFrom(src => src.Part.PartNumber))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Part.Price));
            CreateMap<InquiryDetailDto, InquiryDetail>(MemberList.Destination);

            CreateMap<Quotation, QuotationDto>()
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Customer.Company.Name))
                .ForMember(dest => dest.CompanyAddress, opt => opt.MapFrom(src => src.Customer.Company.CompanyAddress))
                .ForMember(dest => dest.CurrencyName, opt => opt.MapFrom(src => src.Currency.Code));
            CreateMap<QuotationDto, Quotation>(MemberList.Destination);
            CreateMap<Quotation, QuotationListItem>(MemberList.Destination)
                .ForMember(dest => dest.CustomerName, opt => opt.MapFrom(src => src.Customer.Name))
                .ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Customer.Company.Name))
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(src => src.Currency.Code))
                .ForMember(dest => dest.CreatedByFullName, opt => opt.MapFrom(src => src.CreatedByFullName))
                .ForMember(dest => dest.UpdatedByFullName, opt => opt.MapFrom(src => src.UpdatedByFullName));

            CreateMap<QuotationDetail, QuotationDetailDto>()
                .ForMember(dest => dest.PartNumber, opt => opt.MapFrom(src => src.Part.PartNumber));
            CreateMap<QuotationDetailDto, QuotationDetail>(MemberList.Destination);

            CreateMap<CategoryDto, Category>().ReverseMap();
            CreateMap<Category, CategoryLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CategoryId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.CategoryName));

            CreateMap<PartDto, Part>().ReverseMap();
            CreateMap<Part, PartLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.PartId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.PartNumber, opt => opt.MapFrom(src => src.PartNumber))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price));

            CreateMap<UserDto, SalesPersonLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.FullName));

            CreateMap<Reason, ReasonLookup>(MemberList.Destination)
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ReasonId))
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name));
        }
    }
}