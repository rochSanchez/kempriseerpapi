﻿using ERP.WebApi.Business.Services.ErpEmailService.Sales;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.WebApi.Business.Services.ErpEmailService
{
    public static class ErpEmailServiceRegistration
    {
        public static IServiceCollection AddErpEmailService(this IServiceCollection services)
        {
            services.AddTransient<IInquiryEmailService, InquiryEmailService>();
            services.AddTransient<IQuotationEmailService, QuotationEmailService>();

            return services;
        }
    }
}