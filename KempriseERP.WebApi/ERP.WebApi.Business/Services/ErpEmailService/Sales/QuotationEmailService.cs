﻿using ERP.WebApi.Business.Services.BackgroundTask;
using ERP.WebApi.Business.Services.Builders.Sales;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.Constants;
using ERP.WebApi.DTO.Security.Contracts;

namespace ERP.WebApi.Business.Services.ErpEmailService.Sales
{
    public interface IQuotationEmailService
    {
        void SendCheckQuotationNotification(Quotation quotationEntity);

        void SendApprovalQuotationNotification(Quotation quotationEntity);

        void SendQuotationApprovedNotification(Quotation quotationEntity);

        void SendReviseQuotationNotification(Quotation quotationEntity);

        void SendCloseQuotationNotification(Quotation quotationEntity);

        void SendOrderReceivedNotification(Quotation quotationEntity);
    }

    public class QuotationEmailService : ErpEmailServiceBase, IQuotationEmailService
    {
        public QuotationEmailService(IUserManagementService userManagementService, IBackgroundServiceProvider backgroundService) :
            base(userManagementService, backgroundService)
        {
        }

        public void SendCheckQuotationNotification(Quotation quotationEntity)
        {
            var recipients = UserManagementService.GetConcernedUsers(ErpRoles.CheckQuotation).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(quotationEntity.SalesPersonId).Result;

            SendEmails(recipients, "Quotation Created", new CheckQuotationBuilder(quotationEntity, salesPerson));
        }

        public void SendApprovalQuotationNotification(Quotation quotationEntity)
        {
            var recipients = UserManagementService.GetConcernedUsers(ErpRoles.ApproveQuotation).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(quotationEntity.SalesPersonId).Result;

            SendEmails(recipients, "Quotation Waiting For Approval", new ApprovalQuotationBuilder(quotationEntity, salesPerson));
        }

        public void SendQuotationApprovedNotification(Quotation quotationEntity)
        {
            var recipients = UserManagementService.GetConcernedUsers(ErpRoles.CreateQuotation).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(quotationEntity.SalesPersonId).Result;

            SendEmails(recipients, "Quotation Approved", new QuotationApprovedBuilder(quotationEntity, salesPerson));
        }

        public void SendReviseQuotationNotification(Quotation quotationEntity)
        {
            var recipients = UserManagementService.GetConcernedUsers(ErpRoles.CreateQuotation).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(quotationEntity.SalesPersonId).Result;

            SendEmails(recipients, "Revision Requested", new ReviseQuotationBuilder(quotationEntity, salesPerson));
        }

        public void SendCloseQuotationNotification(Quotation quotationEntity)
        {
            var recipients = UserManagementService.GetConcernedUsers(ErpRoles.CreateQuotation).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(quotationEntity.SalesPersonId).Result;

            SendEmails(recipients, "Quotation Closed", new ClosedQuotationBuilder(quotationEntity, salesPerson));
        }

        public void SendOrderReceivedNotification(Quotation quotationEntity)
        {
            var recipients = UserManagementService.GetConcernedUsers(ErpRoles.CreateQuotation).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(quotationEntity.SalesPersonId).Result;

            SendEmails(recipients, "Order Received", new OrderReceivedBuilder(quotationEntity, salesPerson));
        }
    }
}