﻿using ERP.WebApi.Business.Services.BackgroundTask;
using ERP.WebApi.Business.Services.Builders.Sales;
using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Security.Contracts;

namespace ERP.WebApi.Business.Services.ErpEmailService.Sales
{
    public interface IInquiryEmailService
    {
        void SendNewInquiryNotification(Inquiry inquiryEntity);
    }

    public class InquiryEmailService : ErpEmailServiceBase, IInquiryEmailService
    {
        public InquiryEmailService(IUserManagementService userManagementService, IBackgroundServiceProvider backgroundService) :
            base(userManagementService, backgroundService)
        {
        }

        public void SendNewInquiryNotification(Inquiry inquiryEntity)
        {
            var recipients = UserManagementService.GetUserByRoleCategory(RoleCategory.Sales).Result;
            var salesPerson = UserManagementService.GetUserByIdAsync(inquiryEntity.SalesPersonId).Result;

            SendEmails(recipients, "Inquiry Created", new InquiryBuilder(inquiryEntity, salesPerson));
        }
    }
}