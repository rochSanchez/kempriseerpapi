﻿using ERP.WebApi.Business.Services.BackgroundTask;
using ERP.WebApi.DTO.DTOs.Authorization;
using ERP.WebApi.DTO.Security.Contracts;
using MimeKit;
using System.Collections.Generic;

namespace ERP.WebApi.Business.Services.ErpEmailService
{
    public class ErpEmailServiceBase
    {
        protected readonly IUserManagementService UserManagementService;
        private readonly IBackgroundServiceProvider _backgroundService;

        public ErpEmailServiceBase(IUserManagementService userManagementService, IBackgroundServiceProvider backgroundService)
        {
            UserManagementService = userManagementService;
            _backgroundService = backgroundService;
        }

        protected void SendEmails(List<UserDto> recipients, string subject, BodyBuilder bodyBuilder)
        {
            _backgroundService.QueEmails(recipients, subject, bodyBuilder);
        }
    }
}