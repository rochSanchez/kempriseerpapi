﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;

namespace ERP.WebApi.Business.Services.Builders.Sales
{
    public class ReviseQuotationBuilder : BodyBuilder, IMailBodyBuilder
    {
        private readonly Quotation _quotationEntity;
        private readonly UserDto _salesPerson;

        public ReviseQuotationBuilder(Quotation quotationEntity, UserDto salesPerson)
        {
            _quotationEntity = quotationEntity;
            _salesPerson = salesPerson;
            TextBody = BuildTextBody();
        }

        public string BuildTextBody()
        {
            return $@"Dear,

            A Quotation ({_quotationEntity.QuotationNumber}) is requested to be revised.

            Quotation No: {_quotationEntity.QuotationNumber}
            Project Name: {_quotationEntity.ProjectName}
            Customer : {_quotationEntity.Customer.Name}
            Sales Person Assigned: {_salesPerson.FullName}

            Requested By: {_quotationEntity.UpdatedByFullName}

            This is a system generated email, please do not reply.";
        }

        public string RecipientName { get; set; }
    }
}