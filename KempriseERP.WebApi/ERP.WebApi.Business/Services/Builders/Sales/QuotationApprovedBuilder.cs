﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;

namespace ERP.WebApi.Business.Services.Builders.Sales
{
    public class QuotationApprovedBuilder : BodyBuilder, IMailBodyBuilder
    {
        private readonly Quotation _quotationEntity;
        private readonly UserDto _salesPerson;

        public QuotationApprovedBuilder(Quotation quotationEntity, UserDto salesPerson)
        {
            _quotationEntity = quotationEntity;
            _salesPerson = salesPerson;
            TextBody = BuildTextBody();
        }

        public string BuildTextBody()
        {
            return $@"Dear,

            A Quotation ({_quotationEntity.QuotationNumber}) has been approved and is now ready for printing.

            Quotation No: {_quotationEntity.QuotationNumber}
            Project Name: {_quotationEntity.ProjectName}
            Customer : {_quotationEntity.Customer.Name}
            Sales Person Assigned: {_salesPerson.FullName}

            Approved By: {_quotationEntity.UpdatedByFullName}

            This is a system generated email, please do not reply.";
        }

        public string RecipientName { get; set; }
    }
}