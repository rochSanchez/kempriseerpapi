﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;

namespace ERP.WebApi.Business.Services.Builders.Sales
{
    public class CheckQuotationBuilder : BodyBuilder, IMailBodyBuilder
    {
        private readonly Quotation _quotationEntity;
        private readonly UserDto _salesPerson;

        public CheckQuotationBuilder(Quotation quotationEntity, UserDto salesPerson)
        {
            _quotationEntity = quotationEntity;
            _salesPerson = salesPerson;
            TextBody = BuildTextBody();
        }

        public string BuildTextBody()
        {
            return $@"Dear,

            A new Quotation ({_quotationEntity.QuotationNumber}) has been created and is submitted for checking.

            Quotation No: {_quotationEntity.QuotationNumber}
            Project Name: {_quotationEntity.ProjectName}
            Customer : {_quotationEntity.Customer.Name}
            Sales Person Assigned: {_salesPerson.FullName}

            Submitted By: {_quotationEntity.UpdatedByFullName}

            This is a system generated email, please do not reply.";
        }

        public string RecipientName { get; set; }
    }
}