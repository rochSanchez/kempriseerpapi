﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;

namespace ERP.WebApi.Business.Services.Builders.Sales
{
    public class InquiryBuilder : BodyBuilder, IMailBodyBuilder
    {
        private readonly Inquiry _inquiryEntity;
        private readonly UserDto _salesPerson;

        public InquiryBuilder(Inquiry inquiryEntity, UserDto salesPerson)
        {
            _inquiryEntity = inquiryEntity;
            _salesPerson = salesPerson;
            TextBody = BuildTextBody();
        }

        public string BuildTextBody()
        {
            return $@"Dear,

            A new Inquiry ({_inquiryEntity.InquiryNumber}) with below details have been created and is pending for a review.

            Inquiry No: {_inquiryEntity.InquiryNumber}
            Project Name: {_inquiryEntity.ProjectName}
            Due Date: {_inquiryEntity.ExpirationDate?.ToString("dd/MMM/yyyy")}
            Customer : {_inquiryEntity.Customer?.Name}
            Sales Person Assigned: {_salesPerson.FullName}

            Created By: {_inquiryEntity.CreatedByFullName}

            This is a system generated email, please do not reply.";
        }

        public string RecipientName { get; set; }
    }
}