﻿using ERP.WebApi.Data.Entities;
using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;

namespace ERP.WebApi.Business.Services.Builders.Sales
{
    public class ApprovalQuotationBuilder : BodyBuilder, IMailBodyBuilder
    {
        private readonly Quotation _quotationEntity;
        private readonly UserDto _salesPerson;

        public ApprovalQuotationBuilder(Quotation quotationEntity, UserDto salesPerson)
        {
            _quotationEntity = quotationEntity;
            _salesPerson = salesPerson;
            TextBody = BuildTextBody();
        }

        public string BuildTextBody()
        {
            return $@"Dear,

            A Quotation ({_quotationEntity.QuotationNumber}) has been checked and is now waiting for approval.

            Quotation No: {_quotationEntity.QuotationNumber}
            Project Name: {_quotationEntity.ProjectName}
            Customer : {_quotationEntity.Customer.Name}
            Sales Person Assigned: {_salesPerson.FullName}

            Checked By: {_quotationEntity.UpdatedByFullName}

            This is a system generated email, please do not reply.";
        }

        public string RecipientName { get; set; }
    }
}