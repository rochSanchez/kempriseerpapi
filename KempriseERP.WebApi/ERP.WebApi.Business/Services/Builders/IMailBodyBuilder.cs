﻿namespace ERP.WebApi.Business.Services.Builders
{
    public interface IMailBodyBuilder
    {
        string BuildTextBody();

        string RecipientName { get; set; }
    }
}