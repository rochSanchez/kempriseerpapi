﻿using ERP.WebApi.Business.Services.Emails;
using ERP.WebApi.DTO.DTOs.Authorization;
using Hangfire;
using MimeKit;
using System.Collections.Generic;

namespace ERP.WebApi.Business.Services.BackgroundTask
{
    public class BackgroundServiceProvider : IBackgroundServiceProvider
    {
        public void QueEmails(List<UserDto> recipients, string subject, BodyBuilder bodyBuilder)
        {
            BackgroundJob.Enqueue<IMailer>(x => x.SendEmailByRoleCategory(recipients, subject, bodyBuilder));
        }
    }
}