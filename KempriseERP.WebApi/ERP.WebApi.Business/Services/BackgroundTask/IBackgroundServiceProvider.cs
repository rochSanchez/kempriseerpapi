﻿using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;
using System.Collections.Generic;

namespace ERP.WebApi.Business.Services.BackgroundTask
{
    public interface IBackgroundServiceProvider
    {
        void QueEmails(List<UserDto> recipients, string subject, BodyBuilder bodyBuilder);
    }
}