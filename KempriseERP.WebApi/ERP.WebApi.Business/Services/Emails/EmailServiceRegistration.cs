﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ERP.WebApi.Business.Services.Emails
{
    public static class EmailServiceRegistration
    {
        public static IServiceCollection AddEmailServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IMailer, Mailer>();
            services.Configure<SmtpSettings>(options =>
            {
                options.HostAddress = "smtp.office365.com";
                options.HostPort = 587;
                options.HostUsername = "kempriseerp@kemprise.com";
                options.HostPassword = "bpbycmndggkszdnm";
                options.SenderEMail = "<kempriseerp@kemprise.com>";
                options.SenderName = "KempriseERP";
            });

            return services;
        }
    }
}