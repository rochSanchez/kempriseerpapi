﻿using ERP.WebApi.DTO.DTOs.Authorization;
using MimeKit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Services.Emails
{
    public interface IMailer
    {
        Task SendEmailAsync(string email, string subject, BodyBuilder bodyBuilder);

        Task SendEmailByRoleCategory(List<UserDto> recipients, string subject, BodyBuilder bodyBuilder);
    }
}