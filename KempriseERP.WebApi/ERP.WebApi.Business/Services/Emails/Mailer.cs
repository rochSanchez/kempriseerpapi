﻿using ERP.WebApi.DTO.DTOs.Authorization;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ERP.WebApi.Business.Services.Emails
{
    public class Mailer : IMailer
    {
        public readonly IOptions<SmtpSettings> SmtpSettings;

        public Mailer(IOptions<SmtpSettings> smtpSettings)
        {
            SmtpSettings = smtpSettings;
        }

        public Task SendEmailAsync(string to, string subject, BodyBuilder bodyBuilder)
        {
            try
            {
                var email = new MimeMessage { Sender = MailboxAddress.Parse(SmtpSettings.Value.SenderEMail) };
                if (!string.IsNullOrEmpty(SmtpSettings.Value.SenderName))
                    email.Sender.Name = SmtpSettings.Value.SenderName;
                email.From.Add(email.Sender);
                email.To.Add(MailboxAddress.Parse(to));
                email.Subject = subject;
                email.Body = ((BodyBuilder)bodyBuilder).ToMessageBody();

                using (var smtp = new SmtpClient())
                {
                    smtp.Connect(SmtpSettings.Value.HostAddress, SmtpSettings.Value.HostPort,
                        SmtpSettings.Value.HostSecureSocketOptions);
                    smtp.Authenticate(SmtpSettings.Value.HostUsername, SmtpSettings.Value.HostPassword);
                    smtp.Send(email);
                    smtp.Disconnect(true);
                }

                return Task.FromResult(true);
            }
            catch (Exception e)
            {
                throw new InvalidOperationException(e.Message);
            }
        }

        public async Task SendEmailByRoleCategory(List<UserDto> recipients, string subject, BodyBuilder bodyBuilder)
        {
            foreach (var recipient in recipients)
            {
                try
                {
                    var email = new MimeMessage { Sender = MailboxAddress.Parse(SmtpSettings.Value.SenderEMail) };
                    if (!string.IsNullOrEmpty(SmtpSettings.Value.SenderName))
                        email.Sender.Name = SmtpSettings.Value.SenderName;
                    email.From.Add(email.Sender);
                    email.To.Add(MailboxAddress.Parse(recipient.Email));
                    email.Subject = subject;

                    var body = bodyBuilder.TextBody.Replace("Dear", $@"Dear {recipient.LastName}, {recipient.FirstName}");
                    email.Body = new TextPart(TextFormat.Plain) { Text = $@"{body}" };

                    using var smtp = new SmtpClient();
                    await smtp.ConnectAsync(SmtpSettings.Value.HostAddress, SmtpSettings.Value.HostPort, SecureSocketOptions.StartTls);
                    await smtp.AuthenticateAsync(SmtpSettings.Value.HostUsername, SmtpSettings.Value.HostPassword);
                    await smtp.SendAsync(email);
                    await smtp.DisconnectAsync(true);
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException(e.Message);
                }
            }
        }
    }
}